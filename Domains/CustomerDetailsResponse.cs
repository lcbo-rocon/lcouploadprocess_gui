using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Domains
{
    public class CustomerDetailsResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public CustomerUI record { get; set; }
    }
}