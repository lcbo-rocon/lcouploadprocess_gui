using System.Collections.Generic;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Domains
{
    public class W2UIForm
    {
        public string cmd { get; set; }
        public int limit { get; set; }
        public int offset { get; set; }
        public HashSet<SearchObj> searchObjs { get; set; }
        public string searchLogic { get; set; }
        public string searchOperator { get; set; }
        public string sortField { get; set; }
        public string sortDirection { get; set; }
        public List<UpdateRecord> saveRecords { get; set; }

        public override string ToString()
        {
            return "cmd: " + cmd 
            + " limit: " + limit 
            + " offset: " + offset
            + " searchObjs: " + searchObjs?.Count
            + " searchLogic: " + searchLogic
            + " searchOperator: " + searchOperator
            + " sortField: " + sortField
            + " sortDirection: " + sortDirection
            ;
        }
    }
}