using System;
using System.ComponentModel.DataAnnotations;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Domains
{
    public class RoconEnvironmentResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public string hostName {get; set;}
        public string environment {get; set;}
    }
}