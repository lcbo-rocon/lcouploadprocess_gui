
using System.Collections.Generic;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Domains
{
    public class LcoOrderResponse : W2UIBaseResponse
    {
        public LcoOrder excelOrder { get; set; }
    }
}