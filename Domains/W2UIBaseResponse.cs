using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace LCOUploadProcess.Domains
{
    public class W2UIBaseResponse
    {
        public string status { get; set; }
        public int total { get; set; }
        public string message { get; set; }
        public string currentUser { get; set; }
        public bool isAdministrator { get; set; }

        public override string ToString()
        {
            return "status: " + status 
            + " total: " + total
            + " isAdministrator " + isAdministrator;
        }
    }
}