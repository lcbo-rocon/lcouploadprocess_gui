﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Domains
{
    [Serializable]
    [DataContract]
    public class Order : Resource
    {
    
       [JsonProperty("orderHeader")]
       public virtual OrderHeader orderHeader { get; set; }

       [JsonProperty("orderItems")]
       public virtual OrderItemDetail[] orderItems { get; set; }

       [JsonProperty("errorFileName", NullValueHandling = NullValueHandling.Ignore)]
       public string errorFileName { get; set; }

       [JsonProperty("errorFileContent", NullValueHandling = NullValueHandling.Ignore)]
       public string errorFileContent { get; set; }


       // [JsonProperty("errorDetails", NullValueHandling = NullValueHandling.Ignore)]
       //public ErrorDetail errorDetails { get; set; }
    }


    [Serializable]
    //[JsonProperty(DefaultValueHandling)]
    [DataContract]
    public partial class OrderHeader
    {
        /// <summary>
        /// Identify the source system of the order capture - W for webstore, O for woocommerce, C
        /// for eCommerce WCS
        /// </summary>
        [JsonProperty("orderSource", NullValueHandling = NullValueHandling.Ignore)]
        [JsonRequired]
        public string orderSource { get; set; }


        /// <summary>
        /// New Order, Cancel Order, Return Order
        /// </summary>
        [JsonProperty("orderEvent", NullValueHandling = NullValueHandling.Ignore)]
        [JsonRequired]
        [DataMember(Name = "orderEvent")]
        public string orderEvent { get; set; }

        /// <summary>
        /// LCO Order Status to be populated by ROCON in response. Example; W, D, C,O, E
        /// </summary>
        [JsonProperty("orderStatus", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "orderStatus")]
        public string orderStatus { get; set; }

        /// <summary>
        /// LCO Order type. Example: AGY
        /// </summary>
        [JsonProperty("orderType")]
        [JsonRequired]
        [DataMember(Name = "orderType")]
        public string orderType { get; set; }

        /// <summary>
        /// LCO Customer (Agency) identifier
        /// </summary>
        [JsonProperty("customerNumber")]
        [JsonRequired]
        [DataMember(Name = "customerNumber")]
        public string customerNumber { get; set; }

        /// <summary>
        /// B2B Customer Type - Agency/LCO, Licensee, Grocery..etc
        /// </summary>
        [JsonProperty("customerType", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "customerType")]
        public string customerType { get; set; }

        /// <summary>
        /// Order number generated by Order Generation System
        /// </summary>
        [JsonProperty("lcoOrderNumber", NullValueHandling = NullValueHandling.Ignore)]
        [JsonRequired]
        [DataMember(Name = "lcoOrderNumber")]
        public int lcoOrderNumber { get; set; }

        /// <summary>
        /// Order number generated by Order System Of Record, to be populated by ROCON in response
        /// </summary>
        [JsonProperty("sorOrderNumber", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DataMember(Name = "sorOrderNumber")]
        public int sorOrderNumber { get; set; }

        /// <summary>
        /// Order request date in format yyyy-mm-ddT24:MM:SS-0500, to be populated by calling app in
        /// request
        /// </summary>
        [JsonProperty("orderRequestDate", NullValueHandling = NullValueHandling.Ignore)]
        [JsonRequired]
        [DataMember(Name = "orderRequestDate")]
        public string orderRequestDate { get; set; }

        /// <summary>
        /// Order creation date in format yyyy-mm-ddT24:MM:SS-0500, to be populated by ROCON in
        /// response
        /// </summary>
        [JsonProperty("orderCreationDate", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "orderCreationDate")]
        public string orderCreationDate { get; set; }

        /// <summary>
        /// Date when order will be shipped in format yyyy-mm-dd, to be populated by calling app in
        /// request
        /// </summary>
        [JsonProperty("orderRequiredDate", NullValueHandling = NullValueHandling.Ignore)]
        [JsonRequired]
        [DataMember(Name = "orderRequiredDate")]
        public string orderRequiredDate { get; set; }

        /// <summary>
        /// Date when order will be shipped in format yyyy-mm-dd, to be populated by ROCON in response
        /// </summary>
        [JsonProperty("orderShipmentDate", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "orderShipmentDate")]
        public string orderShipmentDate { get; set; }

        [JsonProperty("routeInfo", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "routeInfo")]
        public RouteInfo routeInfo { get; set; }

        [JsonProperty("shipToInfo", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "shipToInfo")]
        public OrderShipToInfo shipToInfo { get; set; }

        /// <summary>
        /// Delivery types accepted by ROCON for Agency/LCO
        /// </summary>
        [JsonProperty("deliveryType", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "deliveryType")]
        public string deliveryType { get; set; }

        [JsonProperty("orderCommentsInfo", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "orderCommentsInfo")]
        public OrderCommentsInfo orderCommentsInfo { get; set; }

        /// <summary>
        /// to be populated by ROCON in response
        /// </summary>
        [JsonProperty("invoiceInfo", NullValueHandling = NullValueHandling.Ignore)]
        public InvoiceInfo invoiceInfo { get; set; }

        /// <summary>
        /// to be populated by ROCON in response
        /// </summary>
        [JsonProperty("trafficPlanningIndicator", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
       // [JsonConverter(typeof(IndicatorConverter))]
        public string trafficPlanningIndicator { get; set; }

        /// <summary>
        /// to be populated by ROCON in response
        /// </summary>
        [JsonProperty("uncommitedItemQuantityIndicator", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
     //   [JsonConverter(typeof(IndicatorConverter))]
        public string uncommitedItemQtyIndicator { get; set; }

        [JsonProperty("airmilesNo", NullValueHandling = NullValueHandling.Ignore)]
        public string airmilesNo { get; set; }

        [JsonProperty("orderPriority", NullValueHandling = NullValueHandling.Ignore)]
        public string orderPriority { get; set; }

        [JsonProperty("hostOrderNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string hostOrderNumber { get; set; }

        [JsonProperty("referenceNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string referenceNumber { get; set; }

        [JsonProperty("carrierId", NullValueHandling = NullValueHandling.Ignore)]
        public string carrierId { get; set; }

        [JsonProperty("serviceLevel", NullValueHandling = NullValueHandling.Ignore)]
        public string serviceLevel { get; set; }

        /// <summary>
        /// Total number of order line items
        /// </summary>
        [JsonProperty("numberOfLineItems", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "numberOfLineItems")]
        public int numberOfLineItems { get; set; }
        // public string paymentMethod { get; set; }

        //[JsonProperty("errorInfo", NullValueHandling = NullValueHandling.Ignore)]
        //public ErrorDetail errorInfo { get; set; }

    }

    [Serializable]
    //[JsonProperty(DefaultValueHandling)]
    [DataContract]
    public class OrderItemDetail : Resource
    {
        //public int itemLineNumber { get; set; }
        //public int itemSku { get; set; }
        //public int itemQuantity { get; set; }
        //public decimal itemSkuPrice { get; set; }

        [JsonProperty("itemLineNumber")]
        [JsonRequired]
        public int lineNumber { get; set; }

        [JsonProperty("itemSku")]
        [JsonRequired]
        public int sku { get; set; }

        [JsonProperty("itemQuantity")]
        [JsonRequired]
        public int quantity { get; set; }

        [JsonIgnore]
        public string item_category { get; set; }

        [JsonProperty("priceInfo", Order = 1)]
        [JsonRequired]
        public OrderItemPriceInfo priceInfo;

        //[JsonProperty("errorInfo", Order = 2, NullValueHandling = NullValueHandling.Ignore)]
        //public ErrorDetail[] errorInfo { get; set; }
    }

    public partial class OrderItemPriceInfo
    {
        [JsonProperty("itemSellingPrice")]
        [JsonRequired]
        public decimal selling_price { get; set; }

        [JsonProperty("itemBasicPrice")]
        [JsonRequired]
        public decimal unit_price { get; set; }

        [JsonProperty("bottleDeposit")]
        [JsonRequired]
        public decimal bottle_deposit { get; set; }

        [JsonProperty("discount")]
        public decimal discount { get; set; }

        [JsonProperty("licMarkup")]
        public decimal liquor_markup { get; set; }

        [JsonProperty("itemHSTAmount")]
        [JsonRequired]
        public decimal hst_tax { get; set; }

        [JsonProperty("itemRetailPrice")]
        public decimal retail_price { get; set; }
    }

    public partial class OrderShipToInfo: Address
    {
        public bool overwriteShiptoInformation { get; set; }
    }

    public partial class OrderCommentsInfo
    {
        [JsonProperty("packingInvoiceComment", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "packingInvoiceComment")]
        public PackingInvoiceComment PackingInvoiceComment { get; set; }

        [JsonProperty("bolComment", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "bolComment")]
        public string BolComment { get; set; }

        [JsonProperty("pickerComment", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "pickerComment")]
        public string PickerComment { get; set; }

        [JsonProperty("shipLabelComment", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "shipLabelComment")]
        public string ShipLabelComment { get; set; }
    }

    public partial class PackingInvoiceComment
    {
        [JsonProperty("textLine1", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "textLine1")]
        public string TextLine1 { get; set; }

        [JsonProperty("textLine2", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "textLine2")]
        public string TextLine2 { get; set; }

        [JsonProperty("textLine3", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "textLine3")]
        public string TextLine3 { get; set; }
    }

    /// <summary>
    /// to be populated by ROCON in response
    /// </summary>
    public partial class InvoiceInfo
    {
        [JsonProperty("invoiceTotalAmount")]
        public decimal InvoiceTotalAmount { get; set; }

        [JsonProperty("invoiceDiscountAmount")]
        public decimal InvoiceDiscountAmount { get; set; }

        [JsonProperty("invoiceBottleDepositAmount")]
        public decimal InvoiceBottleDepositAmount { get; set; }

        [JsonProperty("invoiceHstAmount")]
        public decimal InvoiceHstAmount { get; set; }

        [JsonProperty("invoiceLicMUAmount", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? InvoiceLicMuAmount { get; set; }

        [JsonProperty("invoiceLevyAmount", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? InvoiceLevyAmount { get; set; }

        [JsonProperty("invoiceRetailAmount", NullValueHandling = NullValueHandling.Ignore)]
        public decimal InvoiceRetailAmount { get; set; }

        [JsonProperty("deliveryCharge", NullValueHandling = NullValueHandling.Ignore)]
        public DeliveryCharge DeliveryCharge { get; set; }
    }

    public partial class DeliveryCharge
    {
        [JsonProperty("deliveryChargeType")]
        public string DeliveryChargeType { get; set; }

        //[JsonProperty("deliveryComment", NullValueHandling = NullValueHandling.Ignore)]
        //public string DeliveryComment { get; set; }

        [JsonProperty("deliveryBaseAmount")]
        public decimal DeliveryBaseAmount { get; set; }    

        [JsonProperty("deliveryHstAmount")]
        public decimal DeliveryHstAmount { get; set; }
    }
}
