using System.Collections.Generic;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Domains
{
    public class LcoUserConfigResponse : W2UIBaseResponse
    {
        public List<LcoUserConfig> records { get; set; }
    }
}