﻿using Newtonsoft.Json;
using LCOUploadProcess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCOUploadProcess.Domains
{
    public abstract class Resource : Link
    {
        [JsonIgnore]
        public Link Self { get; set; }

        [JsonProperty("errorInfo", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public virtual ErrorDetail[] errorInfo {get; set;}
    }
}