using LCOUploadProcess.Domains;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Services
{
    public interface ILcoUserConfigService
    {
        LcoUserConfigResponse loadLcoUserConfigs(W2UIForm w2UIForm);
        LcoUserConfigResponse saveLcoUserConfigs(W2UIForm w2UIForm, 
                                    string currentWinUser);
        LcoUserConfigResponse saveLcoUserConfig(W2UIForm w2UIForm, 
                                            string currentWinUser, 
                                            LcoUserConfig lcoUserConfig);
    }
}