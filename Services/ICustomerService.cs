
using LCOUploadProcess.Domains;

namespace LCOUploadProcess.Services
{
    public interface ICustomerService
    {
        CustomerDetailsResponse loadCustomerDetails(string customerNumber);
    }
}