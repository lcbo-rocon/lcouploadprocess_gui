using System.Linq;
using Oracle.ManagedDataAccess.Client;
using LCOUploadProcess.Domains;
using LCOUploadProcess.Utility;
using System.Collections.Generic;
using System;
using System.Data;
using LCOUploadProcess.Models;
using Microsoft.Extensions.Configuration;

using AutoMapper;

namespace LCOUploadProcess.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public CustomerService(IConfiguration config, IMapper mapper)
        {
            _config = config;
            _mapper = mapper;
        }

        private IDbConnection Connection
        {
            get
            {
                var connection = new OracleConnection();
                connection.ConnectionString = _config["oracle_connectionstr"];
                return connection;
            }
        }


        public CustomerDetailsResponse loadCustomerDetails(string customerNumber)
        {
            // double balance = SQLHelperUtils.GetBalance(Connection, customerNumber);
            var sqlSec = "SELECT CU_NO, PROV_SH, CU_NAME, ADD1_SH, ADD2_SH, CITY_SH, COUNTRY, POST_CODE, PHONE_NU, FAX_NU, CONTACT, EMAIL, CU_TYPE, ACTIVE_FL, CREDIT_AMOUNT, PU_ORDER_FL,"
                            + " CU_ATTR5, CU_ATTR6, ACTIVATION_DATE, NORTHERN_YN, DELIVERY_CODE, WHSE, LCO_YN, CARRIER, CUSTOM1, CUSTOM2, CUSTOM3, COMMENTS"
                                + " FROM lcbo.V_CUSTOMER_INFO"
                                + " WHERE trim(CU_NO) = :custNo";
            
            var customers = new List<CustomerUI>();

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var command = new OracleCommand(sqlSec, (OracleConnection)conn);
                command.Parameters.Add(new OracleParameter("custNo", customerNumber));
                customers = GetCustomerList(command, true, 0.0);
            }

            var w2UIResponse = new CustomerDetailsResponse();
            w2UIResponse.record = customers.FirstOrDefault();
            w2UIResponse.status = "success";
            return w2UIResponse;
        }


        private List<CustomerUI> GetCustomerList(OracleCommand command, bool isAdministrator, double balance)
        {
            var customers = new List<CustomerUI>();
            var oraclReader = command.ExecuteReader();

                while (oraclReader.Read())
                {
                    var customer = new CustomerUI();
                    customer.customerNo = Utils.oracleReaderToString(oraclReader["CU_NO"]);
                    customer.province = Utils.oracleReaderToString(oraclReader["PROV_SH"]);
                    customer.customerName = Utils.oracleReaderToString(oraclReader["CU_NAME"]);
                    customer.addressShipping1 = Utils.oracleReaderToString(oraclReader["ADD1_SH"]);
                    customer.addressShipping2 = Utils.oracleReaderToString(oraclReader["ADD2_SH"]);
                    customer.city = Utils.oracleReaderToString(oraclReader["CITY_SH"]);
                    customer.country = Utils.oracleReaderToString(oraclReader["COUNTRY"]);
                    customer.postalCode = Utils.oracleReaderToString(oraclReader["POST_CODE"]);
                    customer.phoneNo = Utils.oracleReaderToString(oraclReader["PHONE_NU"]);
                    customer.faxNo = Utils.oracleReaderToString(oraclReader["FAX_NU"]);
                    customer.contact = Utils.oracleReaderToString(oraclReader["CONTACT"]);
                    customer.email = Utils.oracleReaderToString(oraclReader["EMAIL"]);
                    customer.active = Utils.oracleReaderToString(oraclReader["ACTIVE_FL"]) == "Y";
                    // customer.creditAmount = Utils.oracleReaderToDouble(oraclReader["CREDIT_AMOUNT"]);
                    customer.puOrder = Utils.oracleReaderToString(oraclReader["PU_ORDER_FL"]);
                    
                    customer.activationDate = Utils.oracleReaderToDateTimeString(oraclReader["ACTIVATION_DATE"], "yyyy/MM/dd");
                    customer.nothernSouthern = Utils.oracleReaderToString(oraclReader["NORTHERN_YN"]) == "Y";
                    customer.deliveryCode = Utils.oracleReaderToString(oraclReader["DELIVERY_CODE"]);
                    customer.wHse = Utils.oracleReaderToString(oraclReader["WHSE"]);
                    customer.lcoYN = Utils.oracleReaderToString(oraclReader["LCO_YN"]) == "Y";
                    customer.carrier = Utils.oracleReaderToString(oraclReader["CARRIER"]);
                    customer.custom1 = Utils.oracleReaderToString(oraclReader["CUSTOM1"]);
                    customer.custom2 = Utils.oracleReaderToString(oraclReader["CUSTOM2"]);
                    customer.custom3 = Utils.oracleReaderToString(oraclReader["CUSTOM3"]);
                    customer.comments = Utils.oracleReaderToString(oraclReader["COMMENTS"]);

                    if (isAdministrator)
                    {
                        customer.customerType = Utils.oracleReaderToString(oraclReader["CU_TYPE"]);
                        customer.cuAttr5 = Utils.oracleReaderToString(oraclReader["CU_ATTR5"]);
                        customer.cuAttr6 = Utils.oracleReaderToString(oraclReader["CU_ATTR6"]);
                        customer.recid = customer.customerNo + " | " + customer.cuAttr5 + " | " + customer.cuAttr6;
                    }
                    else
                    {
                        customer.customerType = "";
                        customer.cuAttr5 = "";
                        customer.cuAttr6 = "";
                        customer.recid = customer.customerNo;
                    }
                    customer.customerBalance = Convert.ToDecimal(balance);

                    customers.Add(customer);
                }
                oraclReader.Close();
                return customers;
        }

    }
}