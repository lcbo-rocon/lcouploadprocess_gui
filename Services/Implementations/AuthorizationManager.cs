using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using LCOUploadProcess.Models;
using LCOUploadProcess.Utility;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace LCOUploadProcess.Services
{
    public class AuthorizationManager : IAuthorizationManager
    {
        private readonly IConfiguration _config;

        public AuthorizationManager(IConfiguration config)
        {
            _config = config;
        }

        private SQLiteConnection Connection
        {
            get
            {
                var connection = new SQLiteConnection();
                connection.ConnectionString = _config["connectionstr"];
                return connection;
            }
        }

        public bool isAuthorized(string currentWinUser)
        {
            var table = "USER_CONFIG";
            var sql = $"SELECT count(1) count FROM {table}"
                        + $" WHERE upper(EM_NO) = upper('{currentWinUser}') AND upper(ACTIVE) = 'Y'";

            // Log.Information($"\nSql is: {sql}");

            long numberOfRecords = 0;

            using (SQLiteConnection conn = Connection)
            {
                conn.Open();
                var command = new SQLiteCommand(conn);
                command.CommandText = sql;
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    numberOfRecords = (Int64)reader["count"];
                }
                reader.Close();
            }
            return numberOfRecords != 0;
        }


        private List<LcoUserConfig> getUserConfig(string sql)
        {
            Log.Debug("UserConfig sql: \n" + sql);
            var lcoUserConfigs = new List<LcoUserConfig>();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var command = new SQLiteCommand(sql, (SQLiteConnection)conn);
                var oraclReader = command.ExecuteReader();

                while (oraclReader.Read())
                {
                    var lcoUserConfig = new LcoUserConfig();
                    lcoUserConfig.userconfigId = Utils.oracleReaderDecimalToInt(oraclReader["USER_CONFIG_ID"]);
                    lcoUserConfig.emNo = Utils.oracleReaderToString(oraclReader["EM_NO"]);
                    lcoUserConfig.lcoUserGroup = Utils.oracleReaderToString(oraclReader["LCO_USER_GROUP"]);
                    lcoUserConfig.lcoModule = Utils.oracleReaderToString(oraclReader["LCO_MODULE"]);
                    lcoUserConfig.lcoAction = Utils.oracleReaderToString(oraclReader["LCO_ACTION"]);
                    lcoUserConfig.active = Utils.oracleReaderToString(oraclReader["ACTIVE"]).Equals("Y");
                    lcoUserConfigs.Add(lcoUserConfig);
                }
            }
            return lcoUserConfigs;
        }
        
    }
}