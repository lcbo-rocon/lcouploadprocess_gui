using System.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using LCOUploadProcess.Domains;
using LCOUploadProcess.Models;
using LCOUploadProcess.Utility;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using Serilog;
using Dapper.Oracle;
using Dapper;
using AutoMapper;

namespace LCOUploadProcess.Services
{
    public class LCOUpdateService : ILCOUpdateService
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public LCOUpdateService(IConfiguration config, IMapper mapper)
        {
            _config = config;
            _mapper = mapper;
        }

        private OracleConnection Connection
        {
            get
            {
                var connection = new OracleConnection();
                // Log.Information($"Connection string is: {_config["oracle_connectionstr"]}");
                connection.ConnectionString = _config["oracle_connectionstr"];
                return connection;
            }
        }

        public DataTable excelTemplateData()
        {
            var table = new DataTable();
            var query = "SELECT t1.SKU, t1.DESCRIPTION, t1.SELLING_INCREMENTS, t1.VOLUME, t2.ITEM_CATEGORY_DESC"
                        + " from lcbo.v_lco_items t1"
                        + " inner join dbo.ON_ITEM_CATEGORY T2 ON t1.CATEGORY = trim(t2.ITEM_CATEGORY)"
                        + " WHERE t1.Status = 'Y'"
                        + " ORDER BY ITEM_CATEGORY ASC, TO_NUMBER(SKU) ASC";

            var delimiter = "|";
            var columnNames = "SKU|DESCRIPTION|SELLING INCREMENTS|SIZE|CATEGORY|ORDER QTY";
            string[] colnames = columnNames.Split(delimiter.ToCharArray());

            foreach (string col in colnames)
            {
                table.Columns.Add(col);
            }

            var lcoOrderDetails = new List<LcoOrderDetail>();

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var command = new OracleCommand(query, (OracleConnection)conn);
                lcoOrderDetails = getOrderDetailList(command);

                foreach (var item in lcoOrderDetails)
                {
                    var row = table.NewRow();
                    row["SKU"] = item.sku;
                    row["DESCRIPTION"] = item.description;
                    row["SELLING INCREMENTS"] = item.sellingIncrements;
                    row["SIZE"] = item.size;
                    row["CATEGORY"] = item.category;
                    row["ORDER QTY"] = "";

                    table.Rows.Add(row);
                }
            }
            return table;
        }

        private List<LcoOrderDetail> getOrderDetailList(OracleCommand command)
        {
            var orderDetails = new List<LcoOrderDetail>();
            var oraclReader = command.ExecuteReader();

            while (oraclReader.Read())
            {
                var orderDetail = new LcoOrderDetail();
                orderDetail.sku = Utils.oracleReaderToString(oraclReader["SKU"]);
                orderDetail.description = Utils.oracleReaderToString(oraclReader["DESCRIPTION"]);
                orderDetail.sellingIncrements = Utils.oracleReaderDecimalToInt(oraclReader["SELLING_INCREMENTS"]);
                orderDetail.size = (int)Utils.oracleReaderToDouble(oraclReader["VOLUME"]);
                orderDetail.category = Utils.oracleReaderToString(oraclReader["ITEM_CATEGORY_DESC"]);
                orderDetails.Add(orderDetail);
            }

            return orderDetails;
        }

        public async Task<IEnumerable<Customer>> getCustomerInfo(string custNum)
        {
            var param = new OracleDynamicParameters();
            param.Add("lv_customer", custNum);
            param.Add("ref_customer", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            string storeProc = "LCBO.GET_CUSTOMER_INFO";

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var results = await conn.QueryAsync<CustomerEntity, RouteInfoEntity, CustomerContactEntity,
                       ShipToInfoEntity, BillToInfoEntity, CustomerEntity>
                       (storeProc, map: (cust, route, custc, ship, bill) =>
                       {
                           cust.routeInfo = route;
                           cust.customerContact = custc;
                           cust.shipToInfo = ship;
                           cust.billToInfo = bill;
                           //cust.availableShippingDates = shipd;
                           return cust;
                       }, splitOn: "routeCode, contactName, shipToName, billToName", param: param, commandType: CommandType.StoredProcedure);
                return _mapper.Map<IEnumerable<Customer>>(results.ToArray());
            }
        }

        public async Task<IEnumerable<AvailableShippingDateEntity>> GetAvailableShippingDates(string customerNumber, string deliveryDate)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var param = new OracleDynamicParameters();
                // param.Add("lv_order_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                param.Add("lv_order_date", deliveryDate);
                param.Add("lv_number_of_available", 3);
                //param.Add("lv_route_code", routeCode);
                param.Add("lv_customer", customerNumber);
                param.Add("ref_delivery", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_DELIVERY_DT_BY_ROUTE_V3";

                var results = await conn.QueryAsync<AvailableShippingDateEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);
                return results.ToArray();
            }
        }


        public async Task<IEnumerable<Product>> GetProducts(string custType, string skuList)
        {
            using (IDbConnection conn = Connection)
            {
                string customerFilter = custType;
                string storeProc = "LCBO.GET_ITEMLIST_BY_CUST";

                var param = new OracleDynamicParameters();
                param.Add("ref_sku", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
                param.Add("lv_cust", custType);
                param.Add("lv_sku", skuList);

                string sQuery = storeProc;
                conn.Open();

                var results = await conn.QueryAsync<ProductEntity, PriceInfoEntity, ProductEntity>(sQuery, map: (prod, price) =>
                    {
                        prod.priceInfo = price;
                        return prod;
                    }
                    , splitOn: "custType", param: param, commandType: CommandType.StoredProcedure);

                return _mapper.Map<IEnumerable<Product>>(results.ToArray());
            }
        }


        public async Task<IEnumerable<InventoryInfoEntity>> GetProductInventory(string itemList)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();

                //Need to map back to Product
                var storeProc = "GET_ITEM_INVENTORY_INFO";

                var paramInv = new OracleDynamicParameters();
                paramInv.Add("lv_sku", itemList);
                paramInv.Add("ref_inventory", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                var results = await conn.QueryAsync<InventoryInfoEntity>(storeProc, param: paramInv, commandType: CommandType.StoredProcedure);
                // return _mapper.Map<IEnumerable<ProductInventory>>(results.ToArray());
                return _mapper.Map<IEnumerable<InventoryInfoEntity>>(results.ToArray());
            }
        }


        public int GetNextValue()
        {
            int id = -1;
            string sql = "select LCBO.SEQ_LCO_PO_NUM.NEXTVAL id from dual";

            using (IDbConnection conn = Connection)
            {
                conn.Open();
                id = conn.QueryFirst<int>(sql);
            }
            return id;
        }


        public Task<LcoOrder> ReserveInventory(LcoOrder lcoOrder,
                                            IEnumerable<InventoryInfoEntity> inventoryInfoEntities,
                                            IEnumerable<Product> products,
                                            string currentUser
                                            )
        {
            IDbConnection conn = Connection;
            conn.Open();
            bool isSuccess = false;

            var customer = getCustomerInfo(lcoOrder.lcoCustomerNo).Result.First();
            int lcoOrderNumber = GetNextValue();

            using (var tran = conn.BeginTransaction()) //Or however you get the connection
            {
                try
                {
                    //Insert into dbo.LCO_Orders for new Table
                    string storeProc = "LCBO.RESERVE_LCO_ORDER_HEADER";

                    var param = new OracleDynamicParameters();
                    param.Add("lv_lcoOrderNumber", lcoOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                    param.Add("lv_orderRequestedDate", FormatUtils.ConvertToYYYYMMDDHHMISS(DateTime.Now.ToLongDateString()), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    param.Add("lv_orderSource", "P", dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    param.Add("lv_overwriteShipToInformation", 0, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                    param.Add("lv_overwriteShipToAddress1", customer.shipToInfo.addressLine1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_overwriteShipToAddress2", customer.shipToInfo.addressLine2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_overwriteShipToCity", customer.shipToInfo.city, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_overwriteShipToProvinceCode", customer.shipToInfo.provinceCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_overwriteShipToPhoneNumber", customer.shipToInfo.phoneNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_overwriteShipToName", customer.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_overwriteShipToCountry", customer.shipToInfo.country, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_overwriteShipToPostalCode", customer.shipToInfo.postalCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_DeliveryType", "SHIP_TO_ADDR_ON_FILE", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                    param.Add("lv_routeCode", customer.routeInfo.routeCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                    param.Add("lv_routeStop", customer.routeInfo.routeStops, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                    //param.Add("lv_order_priority", DBNull.Value); // order.orderHeader.orderPriority);
                    param.Add("lv_customerNumber", lcoOrder.lcoCustomerNo, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    param.Add("lv_orderRequiredDate", FormatUtils.ConvertToYYYYMMDD(lcoOrder.deliveryDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                    param.Add("lv_orderShipmentDate", FormatUtils.ConvertToYYYYMMDD(lcoOrder.deliveryDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                    param.Add("lv_orderType", "AGY", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                    param.Add("lv_ShiptoName", customer.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                    param.Add("lv_em_no", currentUser, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    param.Add("lv_sorOrderNumber", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);

                    var newOrderNumber = param.Get<Decimal>("lv_sorOrderNumber");
                    var sorOrderNumber = Decimal.ToInt32(newOrderNumber);
                    lcoOrder.sorOrderNumber = sorOrderNumber;

                    int orderedLineNumbers = 1;
                    foreach (LcoOrderDetail lcoOrderDetail in lcoOrder.records)
                    {
                        if (lcoOrderDetail.adjOrderQty == 0)
                        {
                            // only the one which have value  and not duplicate ...
                            continue;
                        }

                        var product = products.Where(x => x.sku.ToString().Equals(lcoOrderDetail.sku)).First();
                        var inventoryInfoEntity = inventoryInfoEntities.Where(x => x.sku.ToString().Equals(lcoOrderDetail.sku)).First();

                        var paramList = new OracleDynamicParameters();
                        paramList.Add("lv_co_odno", sorOrderNumber.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_line", orderedLineNumbers, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_item", lcoOrderDetail.sku, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_qty", lcoOrderDetail.adjOrderQty, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_total_allocated", lcoOrderDetail.adjOrderQty, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_unit_price", product.priceInfo.unit_price, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                        paramList.Add("lv_co_status", "W", dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                        paramList.Add("lv_em_no", "LCOROC", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_rows_affected", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                        storeProc = "LCBO.RESERVE_LCO_ORDER_DETAILS";

                        conn.Execute(storeProc, param: paramList, commandType: CommandType.StoredProcedure);

                        var rowsaffected = Decimal.ToInt32(paramList.Get<Decimal>("lv_rows_affected"));

                        if (rowsaffected == 1)
                        {
                            orderedLineNumbers++;
                        }

                        //Sanity check that number of items have been inserted into ON_CODTL Table
                        // if (orderedLineNumbers != order.orderItems.Count())
                        // {
                        //     isSuccess = false;
                        // }
                    }

                    tran.Commit();
                    isSuccess = true;
                }
                catch (OracleException ex)
                {
                    Log.Error($"There was an error: {ex.Message}");
                    var sErrorList = new List<ErrorDetail>();

                    if (ex.Number == 1422)
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Order number 1 already exist!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = 1.ToString()
                            }
                        });
                    }
                    else
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Unhandling exception occurred.",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }

                        });

                    }
                    lcoOrder.errorDetails = sErrorList;
                }
                finally
                {
                    if (!isSuccess) tran.Rollback();
                }

                return Task.FromResult(lcoOrder);
            }
        }


        private int getNextInvoiceNo()
        {
            IDbConnection conn = Connection;
            conn.Open();
            //bool isSuccess = false;
            int nextInvNo = 0;
            using (var tran = conn.BeginTransaction())
            {
                var param = new OracleDynamicParameters();
                param.Add("lv_nextInvNo", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_NEXT_INVOICE_NO";

                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    nextInvNo = Decimal.ToInt32(param.Get<Decimal>("lv_nextInvNo"));
                    //No rolling back as this can impact other users getting an inv_no from ROCON

                    tran.Commit();
                    //isSuccess = true;
                }
                catch (Exception ex)
                {
                    var sErrorList = new List<ErrorDetail>();

                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "INVALID_ORDER",
                        errorMessage = "ERROR Generating Invoice Number",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.OTHER.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                }
            }
            return nextInvNo;
        }


        public Task<LcoOrder> CreateOrder(LcoOrder lcoOrder, IEnumerable<Product> productList)
        {
            var invoiceTotalAmount = lcoOrder.records.Sum(lcoOrderDetail => lcoOrderDetail.sellingPrice * lcoOrderDetail.adjOrderQty);
            lcoOrder.invoiceTotalAmount = invoiceTotalAmount;

            var totalFullCases = lcoOrder.records.Sum(lcoOrderDetail => lcoOrderDetail.adjOrderQty / lcoOrderDetail.sellingIncrements);
            lcoOrder.totalFullCases = totalFullCases;

            int nextInvNo = getNextInvoiceNo();
            bool isSuccess = false;

            if (nextInvNo != 0)
            {
                IDbConnection conn = Connection;
                conn.Open();
                using (var tran = conn.BeginTransaction())
                {
                    string storeProc = "LCBO.COMMIT_NEW_ORDER";

                    var parmlist = new OracleDynamicParameters();
                    parmlist.Add("lv_invNumber", nextInvNo, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderNumber", lcoOrder.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_OrderType", "AGY", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_ReturnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                    parmlist.Add("lv_startStage", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    parmlist.Add("lv_invoiceAmount", invoiceTotalAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryCharge", 0m, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryTax1", 0m, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryTax2", 0m, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_fullCases", totalFullCases, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    parmlist.Add("lv_partCases", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);

                    try
                    {
                        conn.Execute(storeProc, param: parmlist, commandType: CommandType.StoredProcedure);

                        var returnCode = Decimal.ToInt32(parmlist.Get<Decimal>("lv_ReturnCode"));

                        if (returnCode == 0)
                        {
                            tran.Commit();
                            isSuccess = true;
                        }
                        else
                        {
                            var sb = new System.Text.StringBuilder();
                            string errTable = "";
                            switch (returnCode)
                            {
                                case 1:
                                    errTable = "DBO.ON_INVOICE";
                                    break;
                                case 2:
                                    errTable = "DBO.ON_INV_CO";
                                    break;
                                case 3:
                                    errTable = "DBO.ON_INV_CODTL";
                                    break;
                                case 4:
                                    errTable = "DBO.ON_INV_CODTL_RULE";
                                    break;
                                default:
                                    errTable = "Unknown";
                                    break;
                            }
                            sb.AppendLine("Store Proc error: LCBO.COMMIT_NEW_ORDER");
                            sb.AppendLine("Return Code:" + returnCode);
                            sb.AppendLine("ERROR writing to " + errTable);
                            throw new Exception(sb.ToString());
                        }

                    }
                    catch (Exception ex)
                    {
                        var sErrorList = new List<ErrorDetail>();

                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Lco order number: 1 already exist!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }

                        });

                    }
                    finally
                    {
                        if (!isSuccess) tran.Rollback();
                    }
                }
            }

            return Task.FromResult(lcoOrder);
        }
    }
}