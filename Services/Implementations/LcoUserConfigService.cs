using System;
using System.Collections.Generic;
using Serilog;
using System.Data;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using LCOUploadProcess.Domains;
using System.Data.SQLite;
using LCOUploadProcess.Models;
using LCOUploadProcess.Utility;

namespace LCOUploadProcess.Services
{
    public class LcoUserConfigService : ILcoUserConfigService
    {
        private readonly IAuthorizationManager _authorizationManager;
        private readonly IConfiguration _config;

        public static Dictionary<string, string> fieldToColumnNameDict = new Dictionary<string, string>()
        {
            {"userconfigId", "USER_CONFIG_ID"},
            {"emNo", "EM_NO"},
            {"lcoUserGroup", "LCO_USER_GROUP"},
            {"lcoModule", "LCO_MODULE"},
            {"lcoAction", "LCO_ACTION"},
            {"active", "ACTIVE"},
            {"createdDateTime", "CREATED_DT_TM"},
            {"createdBy", "CREATED_BY"},
            {"updatedBy", "UPDATED_BY"},
            {"updatedDateTime", "UPDATED_DT_TM"}
        };

        public LcoUserConfigService(IConfiguration config, IAuthorizationManager authorizationManager)
        {
            _config = config;
            _authorizationManager = authorizationManager; 
        }

        private IDbConnection Connection
        {
            get
            {
                var connection = new SQLiteConnection();
                connection.ConnectionString = _config["connectionstr"];
                return connection;
            }
        }

        public LcoUserConfigResponse loadLcoUserConfigs(W2UIForm w2UIForm)
        {
            return searchSec(w2UIForm);
        }

        public LcoUserConfigResponse saveLcoUserConfigs(W2UIForm w2UIForm, string currentWinUser)
        {
            Log.Information("****** Saving lcoUserConfig charge records ...");
            return SaveRecords(w2UIForm, currentWinUser);
        }

        private LcoUserConfigResponse SaveRecords(W2UIForm w2UIForm, string currentWinUser)
        {
            var w2UIResponse = new LcoUserConfigResponse();

            if (_authorizationManager.isAuthorized(currentWinUser))
            {
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var sqliteConnection = (SQLiteConnection)conn;
                    var command = sqliteConnection.CreateCommand();
                    Log.Information($"LcoUserConfigService Connection is: {sqliteConnection}");
                    var transaction = sqliteConnection.BeginTransaction();
                    command.Transaction = transaction;

                    try
                    {
                        foreach (UpdateRecord updateRecord in w2UIForm.saveRecords)
                        {
                            var sql = $"Update USER_CONFIG set {updateRecord.getField()} = '{updateRecord.getValue().Replace("'", "")}'"
                                + $", UPDATED_BY = '{currentWinUser}', UPDATED_DT_TM = {Utils.getLastUpdateSqlDateStr()}"
                                + $" where USER_CONFIG_ID = '{updateRecord.itemKey}'";

                            Log.Information("=====> update sql: " + sql);
                            command.CommandText = sql;
                            int updated = command.ExecuteNonQuery();
                            Log.Debug($"Number of rows update: {updated}");

                        }
                        transaction.Commit();
                        w2UIResponse.status = "success";
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Log.Information(e.ToString());
                        Log.Information("Neither record was written/update/delete to database.");
                        w2UIResponse.status = "error";
                        w2UIResponse.message = "There was a general error. See your Administrator";

                        if (e.Message.Contains("are not allow to"))
                        {
                            w2UIResponse.message = e.Message;
                        }
                    }

                }
            }
            else
            {
                // not authorize to approve ...
                var errorMessage = $"You user [{currentWinUser}] are not allow to update!";
                w2UIResponse.message = errorMessage;
                w2UIResponse.status= "error";
            }

            return w2UIResponse;
        }

        private LcoUserConfigResponse searchSec(W2UIForm w2UIForm)
        {
            var searchQuerySec = SQLHelperUtils.getSearchQuerySec(w2UIForm);
            var sqlSec = "SELECT USER_CONFIG_ID, EM_NO, LCO_USER_GROUP, LCO_MODULE, LCO_ACTION, ACTIVE, "
             + " CREATED_BY, CREATED_DT_TM, UPDATED_BY, UPDATED_DT_TM"
              + " FROM  ("
            + " select USER_CONFIG.*,"
            + " row_number() over " + SQLHelperUtils.orderBy(w2UIForm,
                                                            "(ORDER BY {0} {1})",
                                                            "(ORDER BY USER_CONFIG_ID, LCO_USER_GROUP, LCO_MODULE)")
            + " line_number"
            + "   FROM USER_CONFIG "
            + searchQuerySec
            + ") WHERE line_number between " + w2UIForm.offset + " AND " + (w2UIForm.limit + w2UIForm.offset)
            ;
            Log.Information("=====> select sql secure: " + sqlSec);

            var lcoUserConfigs = new List<LcoUserConfig>();

            using (IDbConnection conn = Connection)
            {
                conn.Open();

                var command = new SQLiteCommand(sqlSec, (SQLiteConnection)conn);
                var searchQuery = SQLHelperUtils.populateCommand(w2UIForm, searchQuerySec);
                SQLHelperUtils.populateCommand(w2UIForm, searchQuery);

                var oraclReader = command.ExecuteReader();

                while (oraclReader.Read())
                {
                    var lcoUserConfig = new LcoUserConfig();
                    lcoUserConfig.recid = ((decimal)oraclReader["USER_CONFIG_ID"]).ToString();
                    lcoUserConfig.userconfigId = Int32.Parse(lcoUserConfig.recid);
                    lcoUserConfig.emNo = Utils.oracleReaderToString(oraclReader["EM_NO"]);
                    lcoUserConfig.lcoUserGroup = Utils.oracleReaderToString(oraclReader["LCO_USER_GROUP"]);
                    lcoUserConfig.createdBy = Utils.oracleReaderToString(oraclReader["CREATED_BY"]);
                    lcoUserConfig.createdDateTime = Utils.oracleReaderToDateTimeString(oraclReader["CREATED_DT_TM"], "yyyy/MM/dd hh:mm tt");
                    lcoUserConfig.updatedBy = Utils.oracleReaderToString(oraclReader["UPDATED_BY"]);
                    lcoUserConfig.updatedDateTime = Utils.oracleReaderToDateTimeString(oraclReader["UPDATED_DT_TM"], "yyyy/MM/dd hh:mm tt");
                    lcoUserConfig.lcoModule = Utils.oracleReaderToString(oraclReader["LCO_MODULE"]);
                    lcoUserConfig.active = Utils.oracleReaderToString(oraclReader["ACTIVE"]).Equals("Y");
                    lcoUserConfig.lcoAction = Utils.oracleReaderToString(oraclReader["LCO_ACTION"]);
                    lcoUserConfigs.Add(lcoUserConfig);
                }
                oraclReader.Close();
            }

            Log.Information($"Resulting size of secure: {lcoUserConfigs.Count}");

            var w2UIResponse = new LcoUserConfigResponse();
            w2UIResponse.records = lcoUserConfigs;
            w2UIResponse.total = getRecordCountSec(searchQuerySec, w2UIForm);
            w2UIResponse.status = "success";
            return w2UIResponse;
        }

        private int getRecordCountSec(string searchQuery, W2UIForm w2UIForm)
        {
            var sql = "SELECT count(USER_CONFIG_ID) count"
                      + " FROM USER_CONFIG"
                      + searchQuery;

            int numberOfRecords = 0;
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var command = new SQLiteCommand(sql, (SQLiteConnection)conn);
                searchQuery = SQLHelperUtils.populateCommand(w2UIForm, searchQuery);

                var oraclReader = command.ExecuteReader();

                while (oraclReader.Read())
                {
                    numberOfRecords = Decimal.ToInt32(((Decimal)oraclReader["count"]));
                }
                oraclReader.Close();
            }

            return numberOfRecords;
        }

        public LcoUserConfigResponse saveLcoUserConfig(W2UIForm w2UIForm,
                                                    string currentWinUser,
                                                    LcoUserConfig lcoUserConfig)
        {
            return SaveRecord(w2UIForm, currentWinUser, lcoUserConfig);
        }

        private LcoUserConfigResponse SaveRecord(W2UIForm w2UIForm, string currentWinUser, LcoUserConfig lcoUserConfig)
        {
            var w2UIResponse = new LcoUserConfigResponse();

            var count = getRecordCountExistance(lcoUserConfig);
            if (count == 0)
            {
                string sql = "INSERT INTO USER_CONFIG " +
                                "(EM_NO, LCO_USER_GROUP, LCO_MODULE, LCO_ACTION, ACTIVE, CREATED_BY, CREATED_DT_TM, UPDATED_BY, UPDATED_DT_TM) " +
                                "VALUES (" +
                                  $" '{lcoUserConfig.emNo}', '{lcoUserConfig.lcoUserGroup}', '{lcoUserConfig.lcoModule}',"
                                + $" '{lcoUserConfig.lcoAction}', '{Utils.boolToString(lcoUserConfig.active)}', '{currentWinUser}',"
                                + $" {Utils.getLastUpdateSqlDateStr()}, '{currentWinUser}', {Utils.getLastUpdateSqlDateStr()}"
                                + ")";

                Log.Information("=====> insert sql: " + sql);

                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var command = new SQLiteCommand(sql, (SQLiteConnection)conn);
                    int inserted = command.ExecuteNonQuery();
                    Log.Debug($"Number of rows inserted: {inserted}");
                }

                w2UIResponse.status = "success";
            }
            else
            {
                w2UIResponse.status = "error";
                var message = "The lco user config combination. ";
                message = message + $"User Id: '{lcoUserConfig.emNo}', User group: '{lcoUserConfig.lcoUserGroup}',"
                            + $" Module: '{lcoUserConfig.lcoModule}'.";
                message = message + "\nAlready exist!";
                w2UIResponse.message = message;
            }

            return w2UIResponse;
        }

        private int getRecordCountExistance(LcoUserConfig lcoUserConfig)
        {
            var sql = "SELECT count(*) count"
                      + " FROM USER_CONFIG"
                      + " where UPPER(EM_NO) = UPPER(:EM_NO)"
                      + " AND LCO_USER_GROUP = :LCO_USER_GROUP"
                      + " AND LCO_MODULE = :LCO_MODULE"
                      ;

            int numberOfRecords = 0;          
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var command = new SQLiteCommand(sql, (SQLiteConnection)conn);
                command.Parameters.Add(new SQLiteParameter("EM_NO", lcoUserConfig.emNo));
                command.Parameters.Add(new SQLiteParameter("LCO_USER_GROUP", lcoUserConfig.lcoUserGroup));
                command.Parameters.Add(new SQLiteParameter("LCO_MODULE", lcoUserConfig.lcoModule));

                var oraclReader = command.ExecuteReader();
                while (oraclReader.Read())
                {
                    numberOfRecords = Decimal.ToInt32(((Decimal)oraclReader["count"]));
                }

                oraclReader.Close();
            }

            return numberOfRecords;
        }
    }
}