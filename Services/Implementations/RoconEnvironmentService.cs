using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using LCOUploadProcess.Models;
using LCOUploadProcess.Domains;
using LCOUploadProcess.Utility;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using Serilog;
using System.Text.RegularExpressions;
using System.Net;

namespace LCOUploadProcess.Services
{
    public class RoconEnvironmentService : IRoconEnvironmentService
    {
        private readonly IConfiguration _config;

        public RoconEnvironmentService(IConfiguration config)
        {
            _config = config;
        }

        // private OracleConnection Connection
        // {
        //     get
        //     {
        //         var connection = new OracleConnection();
        //         connection.ConnectionString = _config["oracle_connectionstr"];
        //         return connection;
        //     }
        // }

        public RoconEnvironmentResponse getCurrentEnvironment()
        {
           // var roconEnvResponse = new RoconEnvironment
            var currentEnv = new RoconEnvironmentResponse();
            var parseHostRegEx = @"HOST\s*=\s*([^\)]+)";
            
                      
            var connstr = _config["oracle_connectionstr"];
            var matches = Regex.Matches(connstr, parseHostRegEx, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            
                       
            Log.Information($"\nConnection is: {connstr}");
           
            if (matches.Count > 0)
            {
                 Log.Information($"\nHost is: {matches[0]}");
                var hostname = matches[0].Groups[1].Value.ToString();
                
                //If it's IP address - lookup the domain name
                if (Regex.IsMatch(hostname,@"\d+\.\d+\.\d+.\d+"))
                {
                    IPAddress addr = IPAddress.Parse(hostname);
                    IPHostEntry entry = Dns.GetHostEntry(addr);
                    hostname = entry.HostName;
                }

                var hostnameParts = hostname.Split(".");

                currentEnv.hostName = hostnameParts[0];
                currentEnv.environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                currentEnv.status = "200";
                currentEnv.message = "";
            }
            else
            {
              throw(new Exception("connection string not formatted correctly to determine environment!"));
            }
            

            return currentEnv;
        }

       
               
    }
}