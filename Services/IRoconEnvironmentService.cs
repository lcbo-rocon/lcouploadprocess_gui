using LCOUploadProcess.Domains;

namespace LCOUploadProcess.Services
{
    public interface IRoconEnvironmentService
    {
        RoconEnvironmentResponse getCurrentEnvironment();
    }
}
