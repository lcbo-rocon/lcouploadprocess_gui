using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using LCOUploadProcess.Domains;
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Services
{
    public interface ILCOUpdateService
    {
        DataTable excelTemplateData();

        Task<IEnumerable<Customer>> getCustomerInfo(string custNum);

        Task<IEnumerable<AvailableShippingDateEntity>> GetAvailableShippingDates(string customerNumber, string deliveryDate);

        Task<IEnumerable<Product>> GetProducts(string custType, string skuList);

        Task<IEnumerable<InventoryInfoEntity>> GetProductInventory(string itemList);

        Task<LcoOrder> ReserveInventory(LcoOrder lcoOrder,
                                            IEnumerable<InventoryInfoEntity> inventoryInfoEntities,
                                            IEnumerable<Product> products,
                                            string currentUser
                                            );
        Task<LcoOrder> CreateOrder(LcoOrder lcoOrder, IEnumerable<Product> productList);
    }
}