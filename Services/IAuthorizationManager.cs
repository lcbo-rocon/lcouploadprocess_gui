
using LCOUploadProcess.Models;

namespace LCOUploadProcess.Services
{
    public interface IAuthorizationManager
    {
        bool isAuthorized(string currentWinUser);
    }
}
