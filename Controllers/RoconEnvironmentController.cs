
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using LCOUploadProcess.Services;
using System.Security.Principal;
using LCOUploadProcess.Domains;
using Microsoft.AspNetCore.Http;


namespace LCOUploadProcess.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class RoconEnvironmentController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRoconEnvironmentService _roconEnvironmentService;

        public RoconEnvironmentController(IConfiguration configuration,
                             IHttpContextAccessor httpContextAccessor,
                             IRoconEnvironmentService roconEnvironmentService)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _roconEnvironmentService = roconEnvironmentService;
        }

        [HttpGet]
        public Task<string> GetEnvironment()
        {
            var curEnv = new RoconEnvironmentResponse();
            try
            {
                curEnv =  _roconEnvironmentService.getCurrentEnvironment();
            }
            catch(Exception e)
            {
                Log.Information(e.ToString());
                Log.Information("Could not determine which environment");
                
                curEnv.status = "500";
                curEnv.message = e.ToString();
            }

            return Task.FromResult(Newtonsoft.Json.JsonConvert.SerializeObject(curEnv));
        }


       
    }
}