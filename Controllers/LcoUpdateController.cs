using System.Linq;
using System.Text;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;
using LCOUploadProcess.Services;
using LCOUploadProcess.Utility;
using System.Security.Principal;
using LCOUploadProcess.Domains;
using System.Collections.Generic;
using System.IO;
using System.Collections.Specialized;
using Microsoft.AspNetCore.Http;
using System.Collections;
using System.Web;
using LCOUploadProcess.Models;
using OfficeOpenXml;
using System.Globalization;
using System.Data;
using OfficeOpenXml.Style;
using System.Drawing;

namespace LCOUploadProcess.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class LcoUpdateController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        // private readonly IGroceryOperationService _groceryOperationService;
        private readonly IAuthorizationManager _authorizationManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILcoUserConfigService _lcoUserConfigService;
        private readonly ILCOUpdateService _lcoUpdateService;

        private readonly ICustomerService _lcoCustomerService;

        public LcoUpdateController(IConfiguration configuration,
                             IAuthorizationManager authorizationManager,
                             IHttpContextAccessor httpContextAccessor,
                             ILcoUserConfigService lcoUserConfigService,
                             ILCOUpdateService lcoUpdateService,
                             ICustomerService lcoCustomerService)
        {
            _configuration = configuration;
            _authorizationManager = authorizationManager;
            _httpContextAccessor = httpContextAccessor;
            _lcoUserConfigService = lcoUserConfigService;
            _lcoUpdateService = lcoUpdateService;
            _lcoCustomerService = lcoCustomerService;
        }


        [HttpPost]
        public Task<string> uploadFile()
        {
            var w2UIResponse = new W2UIBaseResponse();
            try
            {
                hasAuthorization();
                var currentWinUser = Utils.getCurrentWindowsUser(WindowsIdentity.GetCurrent().Name,
                                                                    HttpContext.User.Identity.Name,
                                                                    User.Identity.Name
                                                                    );
                w2UIResponse = loadFile();
                // w2UIResponse.currentUser = currentWinUser;
            }
            catch (LcboException e)
            {
                w2UIResponse.status = "warning";
                w2UIResponse.message = e.Message;
            }
            catch (Exception e)
            {
                Log.Information(e.ToString());
                w2UIResponse.status = "warning";
                w2UIResponse.message = "There was a general error. See your Administrator";
            }
            return Task.FromResult(Newtonsoft.Json.JsonConvert.SerializeObject(w2UIResponse));
        }

        [HttpPost]
        public Task<string> validateCustomer()
        {
            var w2UIResponse = new W2UIBaseResponse();
            try
            {
                hasAuthorization();
                var currentWinUser = Utils.getCurrentWindowsUser(WindowsIdentity.GetCurrent().Name,
                                                                    HttpContext.User.Identity.Name,
                                                                    User.Identity.Name
                                                                    );
                w2UIResponse = validatingCustomer();
                // w2UIResponse.currentUser = currentWinUser;
            }
            catch (LcboException e)
            {
                w2UIResponse.status = "warning";
                w2UIResponse.message = e.Message;
            }
            catch (Exception e)
            {
                Log.Information(e.ToString());
                w2UIResponse.status = "error";
                w2UIResponse.message = Utils.getGeneralErrorMessage();
            }
            return Task.FromResult(Newtonsoft.Json.JsonConvert.SerializeObject(w2UIResponse));
        }

        [HttpPost]
        public Task<string> validateDeliveryDate()
        {
            var w2UIResponse = new W2UIBaseResponse();
            try
            {
                hasAuthorization();
                var currentWinUser = Utils.getCurrentWindowsUser(WindowsIdentity.GetCurrent().Name,
                                                                    HttpContext.User.Identity.Name,
                                                                    User.Identity.Name
                                                                    );
                w2UIResponse = validatingDeliveryDate();
                // w2UIResponse.currentUser = currentWinUser;
            }
            catch (LcboException e)
            {
                w2UIResponse.status = "warning";
                w2UIResponse.message = e.Message;
            }
            catch (Exception e)
            {
                Log.Information(e.ToString());
                w2UIResponse.status = "error";
                w2UIResponse.message = Utils.getGeneralErrorMessage();
            }
            return Task.FromResult(Newtonsoft.Json.JsonConvert.SerializeObject(w2UIResponse));
        }


        [HttpPost]
        public Task<string> submitOrder()
        {
            var w2UIResponse = new W2UIBaseResponse();
            try
            {
                hasAuthorization();
                var currentWinUser = Utils.getCurrentWindowsUser(WindowsIdentity.GetCurrent().Name,
                                                                    HttpContext.User.Identity.Name,
                                                                    User.Identity.Name
                                                                    );
                w2UIResponse = submittingOrder(currentWinUser);
                // w2UIResponse.currentUser = currentWinUser;
            }
            catch (LcboException e)
            {
                w2UIResponse.status = "warning";
                w2UIResponse.message = e.Message;
            }
            catch (Exception e)
            {
                Log.Information(e.ToString());
                w2UIResponse.status = "error";
                w2UIResponse.message = Utils.getGeneralErrorMessage();
            }
            return Task.FromResult(Newtonsoft.Json.JsonConvert.SerializeObject(w2UIResponse));
        }


        [HttpPost]
        public Task<string> createInvoiceExcel()
        {
            string reportPath = "";
            var w2UIResponse = new W2UIBaseResponse();

            try
            {
                hasAuthorization();
                var currentWinUser = Utils.getCurrentWindowsUser(WindowsIdentity.GetCurrent().Name,
                                                                    HttpContext.User.Identity.Name,
                                                                    User.Identity.Name
                                                                    );
                reportPath = creatingInvoiceExcel(currentWinUser);
            }
            catch (LcboException e)
            {
                w2UIResponse.status = "warning";
                w2UIResponse.message = e.Message;
            }
            catch (Exception e)
            {
                Log.Information(e.ToString());
                Log.Information("Neither record was written to database.");
                var response = new LcoOrderResponse();
                response.message = "There was a general error. See your Administrator";
            }
            w2UIResponse.message = reportPath;
            return Task.FromResult(Newtonsoft.Json.JsonConvert.SerializeObject(w2UIResponse));
        }


        [HttpGet]
        public ActionResult generateExcelTemplate()
        {
            string reportPath = "";
            try
            {
                hasAuthorization();
                var currentWinUser = Utils.getCurrentWindowsUser(WindowsIdentity.GetCurrent().Name,
                                                                    HttpContext.User.Identity.Name,
                                                                    User.Identity.Name
                                                                    );
                var dataTable = _lcoUpdateService.excelTemplateData();
                reportPath = generatingExcelTemplate(dataTable);
                // w2UIResponse.currentUser = currentWinUser;
            }
            catch (LcboException e)
            {
                var w2UIResponse = new W2UIBaseResponse();
                w2UIResponse.status = "warning";
                w2UIResponse.message = e.Message;
                return new JsonResult(Newtonsoft.Json.JsonConvert.SerializeObject(w2UIResponse));
            }
            catch (Exception e)
            {
                Log.Information(e.ToString());
                Log.Information("Neither record was written to database.");
                var response = new LcoOrderResponse();
                response.message = "There was a general error. See your Administrator";
                return new JsonResult(response);
            }
            return PhysicalFile(reportPath,
                                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                          "LcoOrderTemplate.xlsx");
        }



        [HttpPost]
        public Task<string> GetCustomerDetails()
        {
            // var w2UIResponse = new CustomerResponse();
            var w2UIResponse = new CustomerDetailsResponse();
            try
            {
                hasAuthorization();
                w2UIResponse = GetCustomerDetailResponse();
            }
            catch (LcboException e)
            {
                var response = new LcoOrderResponse();
                response.status = "warning";
                response.message = e.Message;
            }
            catch (Exception e)
            {
                Log.Information(e.ToString());
                Log.Information("Neither record was written to database.");
                var response = new LcoOrderResponse();
                response.message = "There was a general error. See your Administrator";
            }
            return Task.FromResult(Newtonsoft.Json.JsonConvert.SerializeObject(w2UIResponse));
        }

        // private CustomerResponse GetCustomerDetailResponse()
        private CustomerDetailsResponse GetCustomerDetailResponse()
        {
            var w2UIResponse = new CustomerDetailsResponse();

            var customerNumber = Request.Form["customerNumber"];
            return _lcoCustomerService.loadCustomerDetails(customerNumber);
        }


        private string generatingExcelTemplate(DataTable dataTable)
        {
            var excel = new ExcelPackage();
            var excelWorkbook = excel.Workbook;

            var worksheet = excelWorkbook.Worksheets.Add("Order");

            worksheet.Cells["A6"].LoadFromDataTable(dataTable, true);

            worksheet.Cells["A1"].Value = "LCO Order Form";

            worksheet.Cells["A3"].Value = "LCO#:";

            worksheet.Cells["A4"].Value = "Delivery Date:";

            worksheet.Cells["C4"].Value = "'MM/dd/yyyy";
            // worksheet.Cells["D4"].Value = "Notice the ' in front of MM/dd/yyy to make it a correct text date format";

            //Format to Header Row
            formatExcel(worksheet, dataTable.Columns.Count, dataTable.Rows.Count);

            worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

            //get user's desktop path
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\" + _configuration["AppSettings:ReportPath"];
            Directory.CreateDirectory(path);
            Log.Information("Full path is: " + path);

            string reportname = _configuration["AppSettings:ReportName"] + "." + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            Utils.deleteFiles(path);
            var fullReportPath = path + "\\" + reportname;
            excel.SaveAs(new FileInfo(fullReportPath));
            Log.Information("All Done! Report saved to:" + fullReportPath);
            return fullReportPath;
        }


        private void formatExcel(ExcelWorksheet worksheet, int columnCount, int rowCount)
        {
            worksheet.Cells["A3"].Style.Font.Bold = true;

            worksheet.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
            worksheet.Cells["B3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

            worksheet.Cells["A4"].Style.Font.Bold = true;

            worksheet.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
            worksheet.Cells["B4"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

            worksheet.Cells["B4"].Style.Numberformat.Format = "MM/dd/yyyy";

            worksheet.Cells["C4"].Value = "MM/dd/yyyy";
            worksheet.Cells["C4"].Style.Font.Italic = true;

            using (var range = worksheet.Cells[1, 1, 1, columnCount])
            {
                range.Style.Font.Size = 17;
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                for (int i = 1; i < range.Columns; ++i)
                {
                    worksheet.Column(i).BestFit = true;
                }
            }

            worksheet.Row(1).Height = worksheet.Row(1).Height * 2;
            worksheet.Row(1).Style.WrapText = true;
            worksheet.Cells["A1:F1"].Merge = true;

            using (var range = worksheet.Cells["A6:H6"])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                // range.Style.Font.Color.SetColor(Color.White);
                range.Style.Font.Size = 11;
                range.Style.Font.Bold = true;
                for (int i = 1; i < range.Columns; ++i)
                {
                    worksheet.Column(i).BestFit = true;
                }
            }

            for (int i = 0; i < rowCount; ++i)
            {
                if (i % 2 == 0)
                {
                    worksheet.Row(i + 7).Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Row(i + 7).Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                }
            }

            worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
        }


        private LcoOrderResponse validatingCustomer()
        {
            var w2UIResponse = new LcoOrderResponse();
            var customerNo = Request.Form["customerNo"];
            var lcoOrder = new LcoOrder();
            setCustomerNumber(customerNo, lcoOrder);
            w2UIResponse.excelOrder = lcoOrder;
            w2UIResponse.status = "success";
            return w2UIResponse;
        }


        private LcoOrderResponse validatingDeliveryDate()
        {
            var w2UIResponse = new LcoOrderResponse();
            var deliveryDate = Request.Form["deliveryDate"];
            var custNum = Request.Form["customerNo"];
            var lcoOrder = new LcoOrder();
            setDeliveryDate(deliveryDate, custNum, lcoOrder);
            w2UIResponse.excelOrder = lcoOrder;
            w2UIResponse.status = "success";
            return w2UIResponse;
        }


        private string creatingInvoiceExcel(string currentUser)
        {
            var lcoOrder = new LcoOrder();

            setDeliveryDate(Request.Form["deliveryDate"], Request.Form["customerNo"], lcoOrder);
            setCustomerNumber(Request.Form["customerNo"], lcoOrder);
            lcoOrder.totalFullCases = Int32.Parse(Request.Form["orderTotalCases"]);
            lcoOrder.sorOrderNumber = Int32.Parse(Request.Form["sorOrderNumber"]);
            lcoOrder.invoiceTotalAmount = Convert.ToDecimal(Request.Form["orderTotalAmount"]);

            lcoOrder.records = getLcoOrderDetails();

            var excel = new ExcelPackage();
            var excelWorkbook = excel.Workbook;

            var worksheet = excelWorkbook.Worksheets.Add("Order");

            // loading data to excel ....
            worksheet.Cells["A7"].LoadFromCollection(toInvoiceExcelRecords(lcoOrder.records));

            worksheet.Cells["A1"].Value = "LCO Order Invoice";

            worksheet.Cells["A3"].Value = "LCO#:";

            worksheet.Cells["B3"].Value = lcoOrder.lcoCustomerNo;

            worksheet.Cells["A4"].Value = "Delivery Date:";
            worksheet.Cells["B4"].Value = lcoOrder.deliveryDate.Replace("-", "/");

            // Headers
            worksheet.Cells["A6"].Value = "SKU";
            worksheet.Cells["B6"].Value = "DESCRIPTION";
            worksheet.Cells["C6"].Value = "SELLING INCREMENTS";
            worksheet.Cells["D6"].Value = "SIZE";
            worksheet.Cells["E6"].Value = "CATEGORY";
            worksheet.Cells["F6"].Value = "ORDER QTY";
            worksheet.Cells["G6"].Value = "ADJ ORDER QTY";
            worksheet.Cells["H6"].Value = "COMMENT";

            //Format to Header Row
            formatExcel(worksheet, lcoOrder.records.Count + 1, lcoOrder.records.Count);

            var lastLine = lcoOrder.records.Count + 9;

            var cellNumberA = "A" + lastLine;
            var cellNumberB = "B" + lastLine;
            worksheet.Cells[cellNumberA].Value = "Order #:";
            worksheet.Cells[cellNumberB].Value = lcoOrder.sorOrderNumber;
            worksheet.Cells[cellNumberA].Style.Font.Bold = true;

            lastLine += 1;
            cellNumberA = "A" + lastLine;
            cellNumberB = "B" + lastLine;
            worksheet.Cells[cellNumberA].Value = "Order Total:";
            worksheet.Cells[cellNumberB].Value = lcoOrder.invoiceTotalAmount;
            worksheet.Cells[cellNumberA].Style.Font.Bold = true;

            lastLine += 1;
            cellNumberA = "A" + lastLine;
            cellNumberB = "B" + lastLine;
            worksheet.Cells[cellNumberA].Value = "Total cases:";
            worksheet.Cells[cellNumberB].Value = lcoOrder.totalFullCases;
            worksheet.Cells[cellNumberA].Style.Font.Bold = true;

            //get user's desktop path
            string path = "wwwroot\\" + _configuration["AppSettings:ReportPath"];

            Directory.CreateDirectory(path);
            Log.Information("Full path is: " + path);
            string reportname = _configuration["AppSettings:ReportInvoiceName"] + "." + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            var fullReportPath = path + "\\" + reportname;
            Utils.deleteFiles(path);
            excel.SaveAs(new FileInfo(fullReportPath));
            Log.Information("All Done! Report is saved to:" + fullReportPath);
            return _configuration["AppSettings:ReportPath"] + "\\" + reportname;
        }


        private List<InvoiceExcel> toInvoiceExcelRecords(List<LcoOrderDetail> lcoOrderDetails)
        {
            var invoiceExcelList = new List<InvoiceExcel>();

            foreach (LcoOrderDetail orderDetail in lcoOrderDetails)
            {
                var invoiceExcel = new InvoiceExcel();
                invoiceExcel.sku = orderDetail.sku;
                invoiceExcel.description = orderDetail.description;
                invoiceExcel.sellingIncrements = orderDetail.sellingIncrements;
                invoiceExcel.size = orderDetail.size;
                invoiceExcel.category = orderDetail.category;
                invoiceExcel.orderQty = orderDetail.orderQty;
                invoiceExcel.adjOrderQty = orderDetail.adjOrderQty;
                invoiceExcel.comment = orderDetail.comment;

                invoiceExcelList.Add(invoiceExcel);
            }

            return invoiceExcelList;
        }


        private LcoOrderResponse submittingOrder(string currentUser)
        {
            var w2UIResponse = new LcoOrderResponse();

            var lcoOrder = new LcoOrder();

            setDeliveryDate(Request.Form["deliveryDate"], Request.Form["customerNo"], lcoOrder);
            setCustomerNumber(Request.Form["customerNo"], lcoOrder);

            lcoOrder.records = getLcoOrderDetails();

            w2UIResponse.excelOrder = lcoOrder;
            w2UIResponse.status = "success";

            bool thereIsValidationError = (!lcoOrder.lcoCustomerValid || !lcoOrder.lcoDeliveryDateValid);

            if (thereIsValidationError)
            {
                w2UIResponse.status = "error";
                w2UIResponse.message = "There are input errors. Please fix and resubmit";
                return w2UIResponse;
            }

            int take = Int32.Parse(_configuration["numberOfRecords"]);
            var inventoryInfoEntities = GetInventoryInfoEntities(lcoOrder, take);
            checkInventory(lcoOrder, inventoryInfoEntities);

            var products = GetProducts(lcoOrder, take);

            lcoOrder = _lcoUpdateService.ReserveInventory(lcoOrder,
                                                            inventoryInfoEntities,
                                                            products,
                                                            currentUser).Result;
            lcoOrder.errorDetails = new List<ErrorDetail>();
            if (lcoOrder.errorDetails.Count != 0)
            {
                Log.Error("There was an error calling ReserveInventory");
                printErrorList(lcoOrder, w2UIResponse);
                return w2UIResponse;
            }

            var totalAdjOrderQty = lcoOrder.records.Sum(lcoOrderDetail => lcoOrderDetail.adjOrderQty);
            if (totalAdjOrderQty == 0)
            {
                w2UIResponse.status = "error";
                w2UIResponse.message = "Order not created. There are not Order units found";
                return w2UIResponse;
            }

            lcoOrder = _lcoUpdateService.CreateOrder(lcoOrder, products).Result;

            if (lcoOrder.errorDetails.Count != 0)
            {
                Log.Error("There was an error calling CreateOrder");
                printErrorList(lcoOrder, w2UIResponse);
                return w2UIResponse;
            }

            // lcoOrder.invoiceTotalAmount = 120.20m;
            // lcoOrder.totalFullCases = 2;
            // lcoOrder.sorOrderNumber = 5674;

            return w2UIResponse;
        }

        private void printErrorList(LcoOrder lcoOrderReturn, LcoOrderResponse w2UIResponse)
        {
            var sb = new StringBuilder();
            foreach (ErrorDetail errorDetail in lcoOrderReturn.errorDetails)
            {
                sb.Append(errorDetail.errorMessage + "\n");
            }
            Log.Information(sb.ToString());

            w2UIResponse.message = $"There were errors submitting Order. Check with your Administrator";
            w2UIResponse.status = "error";
        }


        private void checkInventory(LcoOrder lcoOrder,
                                    IEnumerable inventoryInfoEntities)
        {
            foreach (InventoryInfoEntity inventoryInfoEntity in inventoryInfoEntities)
            {
                int availableForSale = inventoryInfoEntity.AvailableForSale;
                var lcoOrderDetail = lcoOrder.records.
                        Where(x => x.sku.Equals(inventoryInfoEntity.sku.ToString())
                        && !x.comment.Contains("Duplicate")).First();

                if (lcoOrderDetail.adjOrderQty == 0)
                {
                    continue;
                }

                if (availableForSale <= 0 || availableForSale < lcoOrderDetail.sellingIncrements)
                {
                    lcoOrderDetail.comment = "Invalid. Item is out of stock and not part of order";
                    lcoOrderDetail.adjOrderQty = 0;
                }
                else if (lcoOrderDetail.adjOrderQty > availableForSale)
                {
                    int calcValue = (int)Math.Floor(Convert.ToDouble(availableForSale) / Convert.ToDouble(lcoOrderDetail.sellingIncrements));
                    int calcOrderQty = calcValue * lcoOrderDetail.sellingIncrements;

                    lcoOrderDetail.comment = "Adjusted. Item quantity adjusted to available stock";
                    lcoOrderDetail.adjOrderQty = calcOrderQty;

                    if (calcOrderQty > availableForSale)
                    {
                        lcoOrderDetail.comment = "Invalid. Item is out of stock and not part of order";
                        lcoOrderDetail.adjOrderQty = 0;
                    }
                }
            }
        }


        private IEnumerable<InventoryInfoEntity> GetInventoryInfoEntities(LcoOrder lcoOrder, int take)
        {
            int step = (int)Math.Ceiling(lcoOrder.records.Count / (double)take);
            int counter = 0;
            var inventoryInfoEntityList = new List<InventoryInfoEntity>();

            for (int i = 0; i < step; ++i)
            {
                var sb = new StringBuilder();
                for (int j = counter; j < counter + take; ++j)
                {
                    try
                    {
                        sb.Append((lcoOrder.records[j]).sku).Append(",");
                    }
                    catch (Exception ex)
                    {
                        Log.Information($"Records finish at: {j} with exception {ex.ToString()}");
                        break;
                    }
                }
                counter = counter + take;

                var inventoryInfoEntities = _lcoUpdateService.GetProductInventory(sb.ToString()).Result;
                inventoryInfoEntityList.AddRange(inventoryInfoEntities);

            }
            return inventoryInfoEntityList;
        }


        private List<LcoOrderDetail> getLcoOrderDetails()
        {
            var lcoOrderDetails = new List<LcoOrderDetail>();

            foreach (string key in Request.Form.Keys)
            {
                string value = key;
                if (value.Contains("record"))
                {
                    var values = value.Split(",");
                    var lcoOrderDetail = new LcoOrderDetail();
                    lcoOrderDetail.recid = values[1];
                    lcoOrderDetail.sku = values[2];
                    lcoOrderDetail.description = values[3];
                    // Log.Information("==> Current sku: " + lcoOrderDetail.sku);
                    lcoOrderDetail.sellingIncrements = Int32.Parse(values[4]);
                    lcoOrderDetail.size = Int32.Parse(values[5]);
                    lcoOrderDetail.category = values[6];
                    lcoOrderDetail.orderQty = Int32.Parse(values[7]);
                    lcoOrderDetail.adjOrderQty = Int32.Parse(values[8]);

                    lcoOrderDetail.comment = values[9];
                    if (values[9].Equals("null"))
                    {
                        lcoOrderDetail.comment = "";
                    }
                    lcoOrderDetail.sellingPrice = Convert.ToDecimal(values[10]);

                    lcoOrderDetails.Add(lcoOrderDetail);
                }
            }
            return lcoOrderDetails;
        }


        private LcoOrderResponse loadFile()
        {
            var w2UIResponse = new LcoOrderResponse();
            var fileName = Request.Form["record[temmplateUpload][0][name]"];
            var type = Request.Form["record[temmplateUpload][0][type]"];
            var modified = Request.Form["record[temmplateUpload][0][modified]"];
            var size = Request.Form["record[temmplateUpload][0][size]"];
            var content = Convert.FromBase64String(Request.Form["record[temmplateUpload][0][content]"]);
            Log.Information("File name: " + fileName);

            var lcoOrder = readUploadExcel(content);

            findDuplicate(lcoOrder);

            updateAjdOrderAndComment(lcoOrder,
                            Int32.Parse(_configuration["numberOfRecords"]));

            w2UIResponse.excelOrder = lcoOrder;
            w2UIResponse.status = "success";
            w2UIResponse.total = lcoOrder.records.Count;
            return w2UIResponse;
        }

        private LcoOrder readUploadExcel(byte[] fileBytes)
        {
            var lcoOrder = new LcoOrder();
            lcoOrder.records = new List<LcoOrderDetail>();
            var lcoOrderDetail = new LcoOrderDetail();

            var duplicateSku = new ArrayList();

            using (MemoryStream stream = new MemoryStream(fileBytes))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                //loop all worksheets
                foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                {
                    //loop all rows
                    for (int i = worksheet.Dimension.Start.Row; i <= worksheet.Dimension.End.Row; i++)
                    {
                        int counter = 0;
                        //loop all columns in a row
                        for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                        {
                            if (worksheet.Cells[i, j].Value != null)
                            {
                                var value = worksheet.Cells[i, j].Value.ToString();
                                if (i == 3 && j == 2)
                                {
                                    setCustomerNumber(value, lcoOrder);
                                }
                                else if (i == 4 && j == 2)
                                {
                                    setDeliveryDate(value, lcoOrder.lcoCustomerNo, lcoOrder);
                                }
                                else if (i > 6) // starting reading detail ...
                                {
                                    if (j == 1 && duplicateSku.Contains(value))
                                    {
                                        counter++;
                                    }
                                    duplicateSku.Add(value);
                                    lcoOrderDetail = setDetail(value, lcoOrder, lcoOrderDetail, j, counter);
                                }
                            }
                        }
                    }
                }
            }
            return lcoOrder;
        }


        private void findDuplicate(LcoOrder lcoOrder)
        {
            var skuList = new HashSet<string>();

            foreach (LcoOrderDetail lcoOrderDetail in lcoOrder.records)
            {
                if (skuList.Contains(lcoOrderDetail.sku))
                {
                    lcoOrderDetail.adjOrderQty = 0;
                    lcoOrderDetail.comment = "Invalid. Duplicate item found. Not part of order";
                }
                skuList.Add(lcoOrderDetail.sku);
            }
        }


        private void updateAjdOrderAndComment(LcoOrder lcoOrder,
                                int take)
        {
            var products = GetProducts(lcoOrder, take);
            validateByProduct(products, lcoOrder);

            var noLongerOfferedProducts = lcoOrder.records.Where(lcoOrderDetail => products.All(product => !lcoOrderDetail.sku.Equals(product.sku.ToString())));
            Log.Information("No longer offered products count: " + noLongerOfferedProducts.Count().ToString());
            foreach (LcoOrderDetail lcoOrderDetail in noLongerOfferedProducts)
            {
                lcoOrderDetail.adjOrderQty = 0;
                lcoOrderDetail.comment = "Invalid. Item is not longer offered in catalog";
            }
        }


        private IEnumerable<Product> GetProducts(LcoOrder lcoOrder, int take)
        {
            int step = (int)Math.Ceiling(lcoOrder.records.Count / (double)take);
            int counter = 0;
            var productList = new List<Product>();

            for (int i = 0; i < step; ++i)
            {
                var sb = new StringBuilder();
                for (int j = counter; j < counter + take; ++j)
                {
                    try
                    {
                        sb.Append((lcoOrder.records[j]).sku).Append(",");
                    }
                    catch (Exception ex)
                    {
                        Log.Information($"Records finish at: {j} with exception: {ex.ToString()}");
                        break;
                    }
                }
                counter = counter + take;

                var products = _lcoUpdateService.GetProducts("A3", sb.ToString()).Result;
                productList.AddRange(products);
            }
            return productList;
        }


        private async void setCustomerNumber(String value, LcoOrder lcoOrder)
        {
            // add checking logic here ...
            lcoOrder.lcoCustomerNo = value;
            lcoOrder.lcoCustomerValid = true;

            var data = await _lcoUpdateService.getCustomerInfo(value);

            if (data.Count() == 0)
            {
                lcoOrder.lcoCustomerValid = false;
                Log.Information($"No customer found for: {value}");
                return;
            }

            var customer = data.First();
            lcoOrder.lcoCustomerValid = (customer.customerStatus.Equals("ACTIVE") && customer.customerType.Equals("AGENCY"));

            if (lcoOrder.lcoCustomerValid)
            {
                lcoOrder.lcoCustomerName = customer.customerName;
                lcoOrder.lcoCustomerAddress = customer.billToInfo.addressLine1;
                lcoOrder.lcoCustomerCity = customer.billToInfo.city;
            }
        }


        private void setDeliveryDate(String value, string custNum, LcoOrder lcoOrder)
        {
            string[] formats = { "MM/dd/yyyy" };
            var deliveryDateTime = DateTime.Now;
            try
            {
                deliveryDateTime = DateTime.ParseExact(value, formats, new CultureInfo("en-US"), DateTimeStyles.None);
            }
            catch (Exception e)
            {
                Log.Warning("Wrong date delivery format: " + e.Message);
            }
            lcoOrder.deliveryDate = deliveryDateTime.ToString("MM/dd/yyyy");

            lcoOrder.deliveryDates = new List<string>();

            lcoOrder.lcoDeliveryDateValid = false;

            foreach (AvailableShippingDateEntity availableShippingDateEntity in
                        _lcoUpdateService.GetAvailableShippingDates(custNum, DateTime.Now.ToString("yyyy-MM-dd")).Result)
            {
                string[] deliverDts = availableShippingDateEntity.DELIVERY_DT.Split(" ");
                string[] dates = deliverDts[0].Split("-");
                var deliveryDTDateTime = new DateTime(Int32.Parse(dates[0]), Int32.Parse(dates[1]), Int32.Parse(dates[2]), 0, 0, 0);

                lcoOrder.deliveryDates.Add(deliveryDTDateTime.ToString("MM/dd/yyyy").Replace("-", "/"));

                int result = DateTime.Compare(deliveryDateTime, deliveryDTDateTime);
                if (result == 0)
                {
                    lcoOrder.lcoDeliveryDateValid = true;
                }
            }
            Log.Information($"lcoOrder.deliveryDates count: {lcoOrder.deliveryDates.Count}");
        }


        private LcoOrderDetail setDetail(String value,
                        LcoOrder lcoOrder,
                        LcoOrderDetail lcoOrderDetail,
                        int col,
                        int counter)
        {
            if (col == 6) // Last column
            {
                if (!String.IsNullOrEmpty(value))
                {
                    // add only if there is value in orderQty ...
                    Log.Debug("RecId: " + lcoOrderDetail.recid + " and value=> [" + value + "]");
                    try
                    {
                        lcoOrderDetail.orderQty = Int32.Parse(value);
                        lcoOrderDetail.adjOrderQty = Int32.Parse(value);
                    }
                    catch (Exception e)
                    {
                        Log.Error($"Error parsing value: [{value}] for recId: {lcoOrderDetail.recid}");
                        Log.Error(e.ToString());
                        return lcoOrderDetail;
                    }
                    lcoOrder.records.Add(lcoOrderDetail);
                }

                lcoOrderDetail = new LcoOrderDetail();
            }
            if (col == 1)
            {
                lcoOrderDetail.sku = value;
                lcoOrderDetail.recid = value + "|" + counter;
            }
            if (col == 2)
            {
                lcoOrderDetail.description = value.Replace("&", "and");
            }
            if (col == 3)
            {
                lcoOrderDetail.sellingIncrements = Int32.Parse(value);
            }
            if (col == 4)
            {
                lcoOrderDetail.size = Int32.Parse(value);
            }
            if (col == 5)
            {
                lcoOrderDetail.category = value;
            }
            return lcoOrderDetail;
        }


        private void validateByProduct(IEnumerable products,
                                        LcoOrder lcoOrder)
        {
            foreach (Product product in products)
            {
                var lcoOrderDetail = lcoOrder.records.Where(x => x.sku.Equals(product.sku.ToString())).First();
                lcoOrderDetail.sellingPrice = product.priceInfo.selling_price;
                if (lcoOrderDetail.sellingIncrements != product.selling_increments)
                {
                    lcoOrderDetail.sellingIncrements = product.selling_increments;
                }

                int ceilingValue = (int)Math.Ceiling(Convert.ToDouble(lcoOrderDetail.orderQty) / Convert.ToDouble(lcoOrderDetail.sellingIncrements));
                int calcOrderQty = ceilingValue * lcoOrderDetail.sellingIncrements;

                if (lcoOrderDetail.adjOrderQty < 0)
                {
                    lcoOrderDetail.adjOrderQty = 0;
                    lcoOrderDetail.comment = "Invalid. Invalid data qty";
                }
                else if (calcOrderQty != lcoOrderDetail.orderQty)
                {
                    lcoOrderDetail.adjOrderQty = calcOrderQty;
                    lcoOrderDetail.comment = "Adjusted. Item qty adjusted to case size";
                }
            }
        }


        private void hasAuthorization()
        {
            var currentWinUser = Utils.getCurrentWindowsUser(WindowsIdentity.GetCurrent().Name,
                                                                HttpContext.User.Identity.Name,
                                                                User.Identity.Name
                                                                );

            if (!_authorizationManager.isAuthorized(currentWinUser))
            {
                throw new LcboException($"You [{currentWinUser}] do not have access. Please submit ServiceNow Request to IT B2B Support for access");
            }
        }
    }
}