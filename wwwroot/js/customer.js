
function loadCustomerDetails(customerNo) {
    console.log("Passed parameter is: " + customerNo);
    w2popup.open({
        title: 'Customer Info: ' + customerNo,
        width: 500,
        height: 400,
        showMax: true,
        body: '<div id="main" style="width: 100%; height: 100%"></div>',
        style: 'padding: 15px 5px 5px 5px',
        // body: '<div id="main" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px;"></div>',
        onOpen: function (event) {
            event.onComplete = function () {
                $('#w2ui-popup #main').w2render('customer_info');
                w2ui.customer_info.postData['customerNumber'] = customerNo;
                w2ui.customer_info.request();
            }
        }
    });
}


$(function () {
    $('#customer_info').w2form({
        name: 'customer_info',
        url: 'lcoUpdate/GetCustomerDetails/',
        style: 'border: 5px; background-color: transparent;',
        formURL: 'pages/customer_info.html',
        fields: [
            { name: 'customerNo', type: 'text', required: false },
            { name: 'customerName', type: 'text', required: false },
            { name: 'customerType', type: 'text', required: false },
            { name: 'deliveryCode', type: 'text', required: false },
            { name: 'wHse', type: 'text', required: false },
            { name: 'active', type: 'checkbox', required: false },
        ],
        actions: {
            reset: function () {
                this.clear();
            },
        },
        onLoad: function(event) {
            console.log('+++ customer_info loading data to form: ' + event);
        },
        onError: function(event) {
            console.log('--- customer_info error data to form: ' + event);
            // w2popup.close();
        }
    });
});
