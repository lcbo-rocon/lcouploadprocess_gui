var userModules = ['DELIVERY RATE', 'DELIVERY SCHEDULE', 'CATALOG ITEM MAINTENANCE', 'CATALOG ITEM INQUIRY', 'CUSTOMER', 'LCO DELIVERY CONFIG'];
var userModulesAdmin = ['LCO USER CONFIG'];
var userModulesSimulator = ['TRAFFIC PLANNING', 'PICK ORDER'];
var userGroups = ['ADMINISTRATOR', 'OPERATOR', 'SIMULATOR'];
var userActions = ['UPDATE', 'READ'];

$(function () {
    $('#lco_user_config').w2form({
        name: 'add_lco_user_config',
        url: 'lcoUserConfig/GetLcoUserConfigs/',
        style: 'border: 0px; background-color: transparent;',
        formURL: 'pages/lco_user_config.html',
        fields: [
            { name: 'emNo', type: 'text', required: true },
            { name: 'active', type: 'checkbox', required: false },
            {
                name: 'lcoUserGroup', type: 'list', required: true,
                options: { items: userGroups }
            },
            {
                name: 'lcoModule', type: 'list', required: true,
                options: { items: userModules }
            },
            {
                name: 'lcoAction', type: 'list', required: true,
                options: { items: userActions }
            },
        ],
        onChange: function (event) {
            console.log('-- on form field change --');
            console.log(event);
            if (event.target === "lcoUserGroup") {
                if (event.value_new.id === "ADMINISTRATOR") {
                    $('input#lcoModule').w2field('list', { items: userModulesAdmin });
                } else if(event.value_new.id === "SIMULATOR") {
                    $('input#lcoModule').w2field('list', { items: userModulesSimulator });
                } else {
                    $('input#lcoModule').w2field('list', { items: userModules });
                }
            }
        },
        onRender: function (event) {
            w2ui['add_lco_user_config'].record['lcoUserGroup'] = '';
            w2ui['add_lco_user_config'].record['lcoModule'] = '';
        },
        actions: {
            "save": function () {
                this.save(function (data) {
                    if (data.status == 'success') {
                        w2ui['lcouserconfig_grid'].reload();
                        $().w2popup('close');
                    }
                    // obj.clear();
                });
            },
            "cancel": function () {
                $().w2popup('close');
                w2ui['lcouserconfig_grid'].reload();
            }
        }
    });
});


function addLcoUserConfig() {
    $().w2popup('open', {
        title: ('Add Lco User Config'),
        body: '<div id="lco_user_config" style="width: 100%; height: 100%"></div>',
        style: 'padding: 15px 5px 5px 5px',
        width: 500,
        height: 350,
        onOpen: function (event) {
            event.onComplete = function () {
                $('#w2ui-popup #lco_user_config').w2render('add_lco_user_config');
            }
        }
    });
}