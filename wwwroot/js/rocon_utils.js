
function foundSideBarPermission(parsedData, sideBarElement) {
    for (var i = 0; i < parsedData.records.length; i++) {
        var record = parsedData.records[i];
        if (record.lcoModule === sideBarElement && record.active === true) {
            return true;
        }
    }
    return false;
}


function datePopup(today, toDate, differenceInDays) {
    w2popup.open({
        title: 'Invalid Date',
        modal: true,
        // body: '</br><div>Total number of days between today date <b>'
        //     + getFormattedDate(today) + '</b> and effective date <b>'
        //     + getFormattedDate(toDate) + '</b> is: <b>'
        //     + differenceInDays + '</b></div></br></br>'
        //     + '<b>Has to be more than 1 day in the future</b>'
        body: '<b>Has to be more than 1 day in the future</b>'
    });
}


function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return year + '-' + month + '-' + day;
}


function getCurrentDatePlus(days) {
    var future = new Date();
    future.setDate(future.getDate() + days);
    return new Date(future.toDateString());
}


function checkDate(strDate, days) {
    var passed = true;
    if (!strDate) {
        // date string is empty ...
        return false;
    }
    var today = new Date(new Date().toDateString());
    // console.log('Date today: ' + today);
    var checkDate = strDate.split("-");
    // new Date(yyyy, mm, dd)
    var toDate = new Date(checkDate[0], checkDate[1] - 1, checkDate[2].split(" ")[0]);
    // console.log('To date is: ' + toDate);
    var differenceInTime = toDate.getTime() - today.getTime();
    var differenceInDays = differenceInTime / (1000 * 3600 * 24);
    // console.log("Difference in days is: " + differenceInDays);
    if (differenceInDays < days) {
        passed = false;
        // datePopup(today, toDate, differenceInDays);
    }
    return passed;
}


function checkEqualityOfDate(dateStr1, dateStr2) {
    var values1 = dateStr1.split(" ");
    var date1 = values1[0];
    var AM_PM1 = values1[2];
    var times1 = values1[1].split(":");
    var hour1 = parseInt(times1[0]);
    var minutes1 = parseInt(times1[1]);

    var values2 = dateStr2.split(" ");
    var date2 = values2[0];
    var AM_PM2 = values2[2];
    var times2 = values2[1].split(":");
    var hour2 = parseInt(times2[0]);
    var minutes2 = parseInt(times2[1]);

    return date1 == date2 && hour1 == hour2 && minutes1 == minutes2 && AM_PM1 == AM_PM2;
}


function formatCells(records, keyValue, checkValue) {
    for (i = 0; i < records.length; i++) {
        var record = records[i];
        if (record[keyValue] === checkValue) {
            console.log("Format cell: ... " + record.recid);
            var style = { customerType: 'background-color: lightgreen; color: darkblue' };
            record["w2ui"] = {
                style: style,
                class: "intro"
            };
            console.log("Record is: " + record["w2ui"]);
        }
    }
}


function copySelectedRows(event, grid) {
    // Ctrl + Z to copy selected rows ...
    if (event.originalEvent.keyCode === 90) {
        console.log("\n\n\n" + grid.copy());
    }
}


function cleanExportData(Data) {
    for (var i = 0; i < Data.length; i++) {
        delete Data[i].recid;
        delete Data[i].w2ui;
    }
}


function exportData(event, records, dataForm, type, showFields) {

    // records       : {}. Can be any data you want to export (records, columns, custom, etc...).
    // type       : string. Extension of file name 'xls' or 'csv' are possible. By default 'excel' format is done on array
    // showFields : boolean (optional). Insert field names on top of the file data. By default 'false'

    cleanExportData(records);

    var arrData = typeof records != 'object' ? JSON.parse(records) : records;
    fileName = 'ExportData.' + type;
    var Data = '';

    var dataForms = dataForm.split("&");


    Data += '\t\t' + 'LCO Order Form';

    Data += '\r\n\r\n' + 'LCO#:' + '\t';
    Data += dataForms[0].split("=")[1] + '\t';

    Data += '\r\n' + 'Delivery Date:' + '\t';
    Data += dataForms[1].split("=")[1] + '\t';

    // show fields on first row ?
    if (showFields) {
        // start table a row 6
        var row = "\r\n\r\n";
        for (var index in arrData[0]) {
            if(index === 'sellingPrice') {
                continue;
            }
            if (row != "" && type == 'csv') row += ',';
            index = translate(index);
            row += index + '\t';
        }
        row = row.slice(0, -1);
        Data += row + '\r\n';
    }

    // Prepare array data format
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        for (var index in arrData[i]) {
            if(index === 'sellingPrice') {
                continue;
            }
            if (row != "" && type == 'csv') row += ',';
            row += (type == 'xls') ? '"' + arrData[i][index] + '"\t' : arrData[i][index] + '\t'
        }
        row = row.replace(/\n/g, "");
        row.slice(0, row.length - 1);
        Data += row + '\r\n';
    }

    Data += '\r\n\r\n' + 'Order number:' + '\t';
    Data += dataForms[2].split("=")[1] + '\t'

    Data += '\r\n\r\n' + 'Order total amount:' + '\t';
    Data += dataForms[3].split("=")[1] + '\t'

    Data += '\r\n\r\n' + 'Order cases:' + '\t';
    Data += dataForms[4].split("=")[1] + '\t'

    // No data?
    if (Data == '') {
        w2alert('No Data Found');
        return;
    }
    var link = document.createElement("a");
    // browser with HTML5 support download attribute
    if (link.download !== undefined) {
        var uri = 'data:application/vnd.ms-excel,' + escape(Data);
        link.setAttribute('href', uri);
        link.setAttribute('style', "visibility:hidden");
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
    // IE 10,11+
    if (navigator.msSaveBlob ||
        navigator.userAgent.match(/edge/i) || navigator.userAgent.match(/trident/i)) {
        var blob = new Blob([Data], {
            "type": "text/csv;charset=utf8;"
        });
        navigator.msSaveBlob(blob, fileName);
    }
}

function translate(index) {
    var new_index = index;
    if (index === "sellingIncrements") {
        new_index = "Selling Increments";
    } else if(index === "orderQty") {
        new_index = "Order Qty";
    } else if(index === "adjOrderQty") {
        new_index = "Adj Order Qty";
    }
    
    new_index = new_index.toUpperCase();
    return new_index.replace(/_/g, " ");
}


function exportDataByKey(event, Data, type, showFields) {
    // Ctrl + X
    if (event.originalEvent.keyCode === 88) {
        exportData(event, Data, type, showFields);
    }
}

//V2.1 Function to display the environment info on the title
function addEnvToTitle(){
    const Http = new XMLHttpRequest();
    Http.onreadystatechange = function(){
        if (this.status == 200 && this.readyState == 4) {
        var objEnv = JSON.parse(Http.responseText);
        console.log(objEnv);
        document.title =  document.title + ' (' + objEnv.hostName.toUpperCase() + ' - ' + objEnv.environment + ')';
        
        }
    };
    const url="RoconEnvironment/GetEnvironment";
    Http.open("GET", url, true);
    Http.send();    
}
