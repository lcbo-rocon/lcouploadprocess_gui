using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using LCOUploadProcess.Domains;
using Newtonsoft.Json;

namespace LCOUploadProcess.Models
{
    [Serializable]
    [DataContract]
    public class LcoOrder
    {
        [JsonProperty("lcoCustomerNo")]
        public string lcoCustomerNo { get; set; }

        [JsonProperty("lcoCustomerName")]
        public string lcoCustomerName { get; set; }

        [JsonProperty("lcoCustomerAddress")]
        public string lcoCustomerAddress { get; set; }


        [JsonProperty("lcoCustomerCity")]
        public string lcoCustomerCity { get; set; }


        [JsonProperty("lcoCustomerValid")]
        public bool lcoCustomerValid { get; set; }

        [JsonProperty("deliveryDate")]
        public string deliveryDate { get; set; }

        [JsonProperty("lcoDeliveryDateValid")]
        public bool lcoDeliveryDateValid { get; set; }

        [JsonProperty("deliveryDates")]
        public List<string> deliveryDates { get; set; }

        [JsonProperty("records")]
        public List<LcoOrderDetail> records { get; set; }

        public List<ErrorDetail> errorDetails { get; set; }

        [JsonProperty("sorOrderNumber")]
        public int sorOrderNumber { get; set; }

        [JsonProperty("totalFullCases")]
        public int totalFullCases { get; set; }

        [JsonProperty("invoiceTotalAmount")]
        public decimal invoiceTotalAmount { get; set; }
    }
}