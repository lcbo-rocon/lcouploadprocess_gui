
namespace LCOUploadProcess.Models
{
    public class CustomerUI
    {
        bool _active;

        public string recid { get; set; }
        public string customerNo { get; set; }
        public string province { get; set; }
        public string customerName { get; set; }
        public string addressShipping1 { get; set; }
        public string addressShipping2 { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string postalCode { get; set; }
        public string phoneNo { get; set; }
        public string faxNo { get; set; }
        public string contact { get; set; }
        public string email { get; set; }
        public string customerType { get; set; }
        public bool active 
        { 
            get
            {
                return _active;
            } 
            set
            {
                _active = value;
            }
        }
        // public double creditAmount { get; set; }
        public string puOrder { get; set; }
        public string cuAttr5 { get; set; }
        public string cuAttr6 { get; set; }
        public string activationDate { get; set; }
        public bool nothernSouthern { get; set; }
        public string deliveryCode { get; set; }
        public string wHse { get; set; }
        public bool lcoYN { get; set; }
        public string carrier { get; set; }
        public string custom1 { get; set; }
        public string custom2 { get; set; }
        public string custom3 { get; set; }
        public string comments { get; set; }
        public decimal customerBalance { get; set; }
    }
}