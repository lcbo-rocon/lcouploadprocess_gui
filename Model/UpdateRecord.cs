
using LCOUploadProcess.Utility;

namespace LCOUploadProcess.Models
{
    public class UpdateRecord
    {
        public string itemKey { get; set; }
        public string field { get; set; }
        public string value { get; set; }

        public string getValue()
        {
            return SQLHelperUtils.getValue(this.value, this.field);
        }

        public string getField()
        {
            return this.field;
        }

        public override string ToString()
        {
            return "field: " + field 
            + " value: " + value 
            + " itemKey: " + itemKey;
            ;
        }
    }
}