using System.Linq;
using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LCOUploadProcess.Models
{
    [Serializable]
    [DataContract]
    public class LcoOrderDetail
    {
        [JsonProperty("recid")]
        public string recid { get; set; }

        [JsonProperty("sku")]
        public string sku { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("sellingIncrements")]
        public int sellingIncrements { get; set; }

        [JsonProperty("size")]
        public int size { get; set; }

        [JsonProperty("category")]
        public string category { get; set; }

        [JsonProperty("orderQty")]
        public int orderQty { get; set; }

        [JsonProperty("adjOrderQty")]
        public int adjOrderQty { get; set; }

        [JsonProperty("sellingPrice")]
        public Decimal sellingPrice { get; set; }

        [JsonProperty("comment")]
        public String comment { get; set; }

        [JsonProperty("w2ui")]
        public StyleRecord w2ui
        {
            get
            {
                var style = new StyleRecord();
                style.style = "";
                if (!String.IsNullOrEmpty(comment) && comment.Contains("Invalid"))
                {
                    style.style = "color:#f2f2f2;background-color:#e60004";
                }
                else if (!String.IsNullOrEmpty(comment) && comment.Contains("Adjusted"))
                {
                    style.style = "color:#f2f2f2;background-color:#ff751a";
                }
                return style;
            }
            set
            {

            }
        }
    }
}