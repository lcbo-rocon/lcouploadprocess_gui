using System;
using System.Collections.Generic;

namespace LCOUploadProcess.Models
{
    public class SearchObj
    {
        public string field { get; set; }
        public string type { get; set; }
        public string operation { get; set; }
        public string value { get; set; }

        public override string ToString()
        {
            return "field: " + field
                    + " type: " + type
                    + " operation: " + operation
                    + " value: " + value
            ;
        }
    }
}