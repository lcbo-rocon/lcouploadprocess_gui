using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LCOUploadProcess.Models
{
    [Serializable]
    [DataContract]
    public class LcoUserConfig
    {
        [JsonProperty("recid")]
        public string recid {get; set; }
        [JsonProperty("userconfigId")]
        public int userconfigId {get; set; } 
        [JsonProperty("emNo")]
        public string emNo { get; set; } 
        [JsonProperty("lcoUserGroup")]
        public string lcoUserGroup { get; set; } 
        [JsonProperty("lcoModule")]
        public string lcoModule { get; set; } 
        [JsonProperty("lcoAction")]
        public string lcoAction { get; set; }
        [JsonProperty("active")]
        public bool active { get; set; }
        [JsonProperty("createdBy")]
        public string createdBy { get; set; }
        [JsonProperty("createdDateTime")]
        public string createdDateTime { get; set; }
        [JsonProperty("updatedBy")]
        public string updatedBy { get; set; }
        [JsonProperty("updatedDateTime")]
        public string updatedDateTime { get; set; }
    }
}