﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using LCOUploadProcess.Models;
using LCOUploadProcess.Domains;

namespace LCOUploadProcess.Models
{
    [DataContract]
    public class OrderEntity 
        {
            public virtual OrderHeaderEntity orderHeader { get; set; }
            public virtual OrderItemsEntity[] orderItems { get; set; }
        }

    public partial class OrderHeaderEntity
    {
        public string orderSource { get; set; }

        public string orderEvent { get; set; }

        public string orderStatus { get; set; }
        public string orderType { get; set; }
          
        public string customerNumber { get; set; }
        public string customerType { get; set; }

        public int lcoOrderNumber { get; set; }

        public int sorOrderNumber { get; set; }

        public string orderRequestDate { get; set; }

        public string orderCreationDate { get; set; }
        public string orderRequiredDate { get; set; }
        public string orderShipmentDate { get; set; }
        public string deliveryType { get; set; }

        public string trafficPlanningIndicator { get; set; }

        public string uncommittedItemQtyIndicator { get; set; }

        public string airmMilesNo { get; set; }

        public string orderPriority { get; set; }

        public string hostOrderNumber { get; set; }

        public string referenceNumber { get; set; }

        public string carrierId { get; set; }

        public string serviceLevel { get; set; }

        public int numberOfLineItems { get; set; }
        public RouteInfoEntity routeInfo { get; set; }
        public OrderShipToInfoEntity shipToInfo { get; set; }

        public OrderCommentsInfoEntity orderCommentsInfo { get; set; }

        public InvoiceInfoEntity invoiceInfo { get; set; }

       // public OrderItemsEntity[] orderItemDetail { get; set; }

        // public string paymentMethod { get; set; }
    }

    public class OrderItemsEntity
    {
        public int lineNumber { get; set; }
        public int sku { get; set; }
        public int quantity { get; set; }
        public string item_category { get; set; }
        public OrderItemPriceInfoEntity priceinfo {get;set;}
    }

    public partial class OrderItemPriceInfoEntity
    {
        public decimal sellingPrice { get; set; }

        public decimal unitPrice { get; set; }

        public decimal bottleDeposit { get; set; }

        public decimal discount { get; set; }

        public decimal licMarkUp { get; set; }

        public decimal hstTax { get; set; }

        public decimal retailPrice { get; set; }
    }

    public partial class OrderShipToInfoEntity : AddressEntity
    {
        public bool overwriteShiptoInformation { get; set; }
     
    }

    public  partial class OrderCommentsInfoEntity
    {
        public string BolComment { get; set; }
        public string PickerComment { get; set; }
        public string ShipLabelComment { get; set; }
        public PackingInvoiceCommentEntity PackingInvoiceComment { get; set; }
    }

    public partial class PackingInvoiceCommentEntity
    {
        public string TextLine1 { get; set; }

        public string TextLine2 { get; set; }

        public string TextLine3 { get; set; }
    }

    public partial class InvoiceInfoEntity
    {
        public decimal InvoiceTotalAmount { get; set; }

        public decimal InvoiceDiscountAmount { get; set; }

        public decimal InvoiceBottleDepositAmount { get; set; }

        public decimal InvoiceHstAmount { get; set; }

        public decimal? InvoiceLicMuAmount { get; set; }

        public decimal? InvoiceLevyAmount { get; set; }

        public decimal InvoiceRetailAmount { get; set; }

        public DeliveryChargeEntity DeliveryCharge { get; set; }
    }

    public partial class DeliveryChargeEntity
    {
        public string DeliveryChargeType { get; set; }


        // This currently doesn't exist
        //public string DeliveryComment { get; set; }

        public decimal DeliveryBaseAmount { get; set; }

        public decimal DeliveryHstAmount { get; set; }
    }
}
