using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AgedARReport.Models
{
    [Serializable]
    [DataContract]

    public class RoconEnvironment
    {
        [JsonProperty("hostName")]
        public string hostName {get; set;}

        [JsonProperty("environment")]
        public string environment {get; set;}
    }
  
}