using System.Linq;
using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LCOUploadProcess.Models
{
    public class InvoiceExcel
    {
        [JsonProperty("sku")]
        public string sku { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("sellingIncrements")]
        public int sellingIncrements { get; set; }

        [JsonProperty("size")]
        public int size { get; set; }

        [JsonProperty("category")]
        public string category { get; set; }

        [JsonProperty("orderQty")]
        public int orderQty { get; set; }

        [JsonProperty("adjOrderQty")]
        public int adjOrderQty { get; set; }

        [JsonProperty("comment")]
        public String comment { get; set; }
    }
}