# LCO Upload Process

## How to run this program

* Project source

dotnet clean

dotnet build

dotnet publish

* To deploy to win065

From the {Project source}\bin\Debug\netcoreapp3.0\publish directory run: 

xcopy /E /I . Y:\WEB\LCOUploadProcess

To copy the SQLite database to win065
From the {Project source}\database directory run: 

xcopy /E /I . Y:\WEB\LCOUploadProcess\database

The {Project source}\database\grocerycrm.db is a SQLite database that contains the user authentication to this web app. You can use DBeaver to connecto to it and make SQL statements. For example:

INSERT INTO USER_CONFIG 
(EM_NO, ACTIVE, CREATED_BY, CREATED_DT_TM, UPDATED_BY, UPDATED_DT_TM)
 VALUES ('ITSZL', 'Y', 'ITSZL', date('now'), 'itszl', date('now'));



## To run it outside IIS do the following:

 * Go to the machine where the application has been copied
 * Open a command prompt as Administrator
 * Make sure donet core framework has been installed in this machine.
 To check if dotnet is installed, open a command prompt and run: dotnet --info
 * The environment variable 'ASPNETCORE_ENVIRONMENT' has to be setup pointing to the correct appsettings.
 This variable setup is inside the 'run.cmd'
 For example: If you have 'appsettings.Development.json' file. You will give a value of 'Development' to the environment variable

 ## In production make sure you use environment variable: Production. Otherwise permissions to use the app will be given to everybody


 * cd {path}\WEB\LCOUploadProcess
 * Run: run.cmd






