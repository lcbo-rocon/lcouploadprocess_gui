using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text.RegularExpressions;
using LCOUploadProcess.Domains;
using LCOUploadProcess.Models;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using Serilog;

namespace LCOUploadProcess.Utility
{
    public class SQLHelperUtils
    {
        public static string getValue(string value, string field)
        {
            var newValue = value;
            if(field.Equals("sellByBottle") || field.Equals("SELL_BY_BOTTLE"))
            {
                newValue = value;
                if(value.Equals("true") || value.Equals("Y"))
                {
                    newValue = "1";
                } 
                else if(value.Equals("false") || value.Equals("N"))
                {
                    newValue = "0";
                } 
            }
            else if(string.Equals("true", value))
            {
                newValue = "'Y'";
            }
            else if(string.Equals("false", value))
            {
                newValue = "'N'";
            }
            return newValue;
        }


        public static string getSearchQuerySec(W2UIForm w2UIForm)
        {
            if (w2UIForm.offset != 0)
            {
                w2UIForm.offset = w2UIForm.offset + 1;
                w2UIForm.limit = w2UIForm.limit - 1;
            }

            var newSearcObjs = new HashSet<SearchObj>();
            string searchQuery = "";
            if (w2UIForm.searchObjs != null)
            {
                int count = 0;
                foreach (SearchObj searchObj in w2UIForm.searchObjs)
                {
                    Log.Information($"Search object is: " + searchObj);
                    var result = getSearchSec(searchObj);
                    if (count == 0)
                    {
                        if (!string.IsNullOrEmpty(result))
                        {
                            searchQuery = $" WHERE {result}";
                            newSearcObjs.Add(searchObj);
                            count++;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(result))
                        {
                            searchQuery = searchQuery + " " + w2UIForm.searchLogic;
                            searchQuery = searchQuery + " " + result;
                            newSearcObjs.Add(searchObj);
                            count++;
                        }
                    }
                }
                // replace with new list ...
                w2UIForm.searchObjs = newSearcObjs;
            }

            Log.Information($"Resulting search secure query: {searchQuery}");
            return searchQuery;
        }
        private static string getSearchSec(SearchObj searchObj)
        {
            string strSearch = null;
            string[] formats = { "yyyy-MM-dd" };
            // bool isInteger = isAInteger(searchObj.value);
            bool isDate = Utils.isADate(searchObj.value, formats);

            if (searchObj.type.Equals("list"))
            {
                return searchByList(searchObj);
            }

            if (searchObj.type.Equals("text"))
            {
                return searchByText(searchObj);
            }

            if (searchObj.type.Equals("int") || searchObj.type.Equals("money")) // && isInteger)
            {
                return searchByInteger(searchObj);
            }

            if (searchObj.type.Equals("date") && isDate)
            {
                return searchByDate(searchObj);
            }

            return strSearch;
        }

        private static string searchByList(SearchObj searchObj)
        {
            string strSearch = null;
            strSearch = $"TRIM({searchObj.field}) = :{searchObj.field}";

            if (searchObj.value.Equals("OTHER"))
            {
                strSearch = $"TRIM({searchObj.field}) <> :{searchObj.field}";
            }
            return strSearch;
        }

        private static string searchByText(SearchObj searchObj)
        {
            string strSearch = null;
            bool containsComma = searchObj.value.Contains(",");

            if (string.Equals(searchObj.operation, "begins"))
            {
                strSearch = $"{searchObj.field} LIKE ':{searchObj.field}%'";
            }
            else if (string.Equals(searchObj.operation, "contains"))
            {
                if (containsComma)
                {
                    string[] values = searchObj.value.Split(',');
                    string result = null;
                    var counter = 0;
                    foreach (string value in values)
                    {
                        if (string.IsNullOrEmpty(result))
                        {
                            result = $"({searchObj.field} LIKE '%:{searchObj.field}{counter}%'";
                        }
                        else
                        {
                            result = result + " OR " + $"{searchObj.field} LIKE '%:{searchObj.field}{counter}%'";
                        }
                        counter++;
                    }
                    result = result + ")";
                    strSearch = result;
                }
                else
                {
                    strSearch = $"{searchObj.field} LIKE '%:{searchObj.field}%'";
                }
            }
            else if (string.Equals(searchObj.operation, "ends"))
            {
                strSearch = $"{searchObj.field} LIKE '%:{searchObj.field}'";
            }
            else if (string.Equals(searchObj.operation, "is"))
            {
                if (containsComma)
                {
                    string[] values = searchObj.value.Split(',');
                    string result = null;
                    var counter = 0;
                    foreach (string value in values)
                    {
                        if (string.IsNullOrEmpty(result))
                        {
                            result = $"(TRIM({searchObj.field}) = ':{searchObj.field}{counter}'";
                        }
                        else
                        {
                            result = result + " OR " + $"TRIM({searchObj.field}) = ':{searchObj.field}{counter}'";
                        }
                        counter++;
                    }
                    result = result + ")";
                    strSearch = result;
                }
                else
                {
                    strSearch = $"TRIM({searchObj.field}) = ':{searchObj.field}'";
                    if (searchObj.value.Equals("OTHER"))
                    {
                        strSearch = $"TRIM({searchObj.field}) <> ':{searchObj.field}'";
                    }
                }
            }
            return strSearch;
        }

        private static string searchByInteger(SearchObj searchObj)
        {
            string strSearch = null;
            Log.Information("\n==> Inside int type ...");
            strSearch = $"{searchObj.field} = :{searchObj.field}";

            if (searchObj.operation.Equals("between"))
            {
                strSearch = $"{searchObj.field} between :{searchObj.field}1 AND :{searchObj.field}2";
            }
            else if (searchObj.operation.Equals("less"))
            {
                strSearch = $"{searchObj.field} < :{searchObj.field}";
            }
            else if (searchObj.operation.Equals("more"))
            {
                strSearch = $"{searchObj.field} > :{searchObj.field}";
            }
            else if (string.Equals(searchObj.operation, "is"))
            {
                strSearch = $"{searchObj.field} = :{searchObj.field}";
            }
            return strSearch;
        }

        private static string searchByDate(SearchObj searchObj)
        {
            string strSearch = null;
            Log.Information("\n==> Inside date type ...");

            if (searchObj.operation.Equals("between"))
            {
                strSearch = $"({searchObj.field} >= TO_DATE(:{searchObj.field}1,'yyyy-mm-dd') AND {searchObj.field} < TO_DATE(:{searchObj.field}2,'yyyy-mm-dd'))";
            }
            else if (searchObj.operation.Equals("less"))
            {
                strSearch = $"({searchObj.field} < TO_DATE(:{searchObj.field},'yyyy-mm-dd'))";
            }
            else if (searchObj.operation.Equals("more"))
            {
                strSearch = $"({searchObj.field} > TO_DATE(:{searchObj.field},'yyyy-mm-dd'))";
            }
            else if (string.Equals(searchObj.operation, "is"))
            {
                strSearch = $"{searchObj.field} = TO_DATE(:{searchObj.field},'yyyy-mm-dd')";
            }
            Log.Information($"searchByDate output value is: [{strSearch}]");
            return strSearch;
        }

        public static string orderBy(W2UIForm w2UIForm,
                                            string orderBy,
                                            string defaultValue)
        {
            if (!string.IsNullOrEmpty(w2UIForm.sortField))
            {
                if (string.IsNullOrEmpty(w2UIForm.sortField))
                {
                    Log.Error($"---- orderBy Sort field {w2UIForm.sortField} was not found");
                    throw new LcboException("SortField is Null or Emtpy");
                }
                else
                {
                    var result = String.Format(orderBy, w2UIForm.sortField, w2UIForm.sortDirection);
                    Log.Information($"\n ===> OrderBy is: {result}");
                    return result;
                }
            }
            return defaultValue;
        }

        public static string populateCommand(W2UIForm w2UIForm, string searchQuery)
        {
            if (w2UIForm.searchObjs != null)
            {
                foreach (SearchObj searchObj in w2UIForm.searchObjs)
                {
                    if (searchObj.operation.Equals("between"))
                    {
                        string[] values = searchObj.value.Split(',');
                        searchQuery = searchQuery.Replace(":" + searchObj.field + 1, values[0]);
                        searchQuery = searchQuery.Replace(":" + searchObj.field + 2, values[1]);
                    }
                    else if (searchObj.type.Equals("text")
                            && (searchObj.operation.Equals("contains") || searchObj.operation.Equals("is"))
                            && searchObj.value.Contains(","))
                    {
                        var values = searchObj.value.Split(',');
                        var counter = 0;

                        foreach (string value in values)
                        {
                            searchQuery = searchQuery.Replace(":" + searchObj.field + "" + counter, value);
                            counter++;
                        }
                    }
                    else
                    {
                        Log.Information($"Value in else from SearchObj is: [{searchObj.value}] for field [{searchObj.field}]");
                        searchQuery = searchQuery.Replace(":" + searchObj.field, searchObj.value);
                    }
                }
            }
            return searchQuery;
        }


        public static double GetBalance(IDbConnection Connection, string custNo)
        {
            OracleCommand cmd = ((OracleConnection) Connection).CreateCommand();
            cmd.CommandText = "LCBO.GET_CUSTOMER_BALANCE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("lv_customer", OracleDbType.Varchar2).Value = custNo;    
            cmd.Parameters.Add("ref_customer", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            double balance = 0.0;
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                OracleDataReader reader1 = ((OracleRefCursor)cmd.Parameters["ref_customer"].Value).GetDataReader();
                // Log.Information($"\n =======> Result is: {reader1.GetName(0)}");
                while(reader1.Read())
                {
                    var result = reader1.GetValue(0);
                    balance = Convert.ToDouble(result);
                }
                Log.Information($"============> Get balance result is: [{balance}]");
            }
            return balance;
        }
    }
}