﻿using AutoMapper;
using LCOUploadProcess.Domains;
using LCOUploadProcess.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCOUploadProcess.Utility
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<OrderEntity, Order>();
               //.ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
               //      Link.To(nameof(Controllers.OrdersController.GetOrderByID),null)));
            CreateMap<OrderHeaderEntity, OrderHeader>()
                .ForMember(dest => dest.orderRequestDate, opt => opt.MapFrom(src =>
                        FormatUtils.AddTimezone(src.orderRequestDate)))
                .ForMember(dest => dest.orderCreationDate, opt => opt.MapFrom(src =>
                         FormatUtils.AddTimezone(src.orderCreationDate)))
                .ForMember(dest => dest.orderRequiredDate, opt => opt.MapFrom(src =>
                         FormatUtils.ConvertToYYYYMMDD(src.orderRequiredDate)))
                .ForMember(dest => dest.orderShipmentDate, opt => opt.MapFrom(src =>
                       FormatUtils.ConvertToYYYYMMDD(src.orderShipmentDate)))
                //.ForMember(dest => dest.orderEvent, opt => opt.MapFrom(src =>
                //       src.orderEvent.ToString()))
            ;

            CreateMap<OrderItemsEntity, OrderItemDetail>();
            CreateMap<OrderItemPriceInfoEntity, OrderItemPriceInfo>();
              

            CreateMap<OrderCommentsInfoEntity, OrderCommentsInfo>();
            CreateMap<PackingInvoiceCommentEntity, PackingInvoiceComment>();
            CreateMap<InvoiceInfoEntity, InvoiceInfo>();
            CreateMap<DeliveryChargeEntity, DeliveryCharge>();

            CreateMap<CustomerEntity, Customer>();
            
            CreateMap<AvailableShippingDateEntity, AvailableShippingDate>()
                .ForMember(dest => dest.cutOffTime, opt => opt.MapFrom(src => FormatUtils.AddTimezone(src.CUTOFF_DT)))
                .ForMember(dest => dest.orderRequiredDate, opt => opt.MapFrom(src => FormatUtils.ConvertToYYYYMMDD(src.DELIVERY_DT)));
            CreateMap<RouteInfoEntity, RouteInfo>();
            CreateMap<CustomerContactEntity, CustomerContact>();
            CreateMap<AddressEntity, Address>();
       
                
            CreateMap<BillToInfoEntity, Address>()
                .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.billToName))
                .ForMember(dest => dest.addressLine1, opt => opt.MapFrom(src => src.billToAddressLine1))
                .ForMember(dest => dest.addressLine2, opt => opt.MapFrom(src => src.billToAddressLine2))
                .ForMember(dest => dest.city, opt => opt.MapFrom(src => src.billToCity))
                .ForMember(dest => dest.provinceCode, opt => opt.MapFrom(src => src.billToProvinceCode))
                .ForMember(dest => dest.country, opt => opt.MapFrom(src => src.billToCountry))
                .ForMember(dest => dest.postalCode, opt => opt.MapFrom(src => src.billToPostalCode))
                .ForMember(dest => dest.phoneNumber, opt=> opt.MapFrom(src=> src.billToPhoneNumber))
                ;

            CreateMap<ShipToInfoEntity, Address>()
               .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.shipToName))
               .ForMember(dest => dest.addressLine1, opt => opt.MapFrom(src => src.shipToAddressLine1))
               .ForMember(dest => dest.addressLine2, opt => opt.MapFrom(src => src.shipToAddressLine2))
               .ForMember(dest => dest.city, opt => opt.MapFrom(src => src.shipToCity))
               .ForMember(dest => dest.provinceCode, opt => opt.MapFrom(src => src.shipToProvinceCode))
               .ForMember(dest => dest.country, opt => opt.MapFrom(src => src.shipToCountry))
               .ForMember(dest => dest.postalCode, opt => opt.MapFrom(src => src.shipToPostalCode))
               .ForMember(dest => dest.phoneNumber, opt => opt.MapFrom(src => src.shipToPhoneNumber))
               ;

            CreateMap<ProductEntity, Product>();

            CreateMap<PriceInfoEntity, PriceInfo>()
                .ForMember(dest => dest.last_update, opt => opt.MapFrom(src =>
                          FormatUtils.AddTimezone(src.last_update)));


            CreateMap<ProductInventoryEntity, ProductInventory>();

            CreateMap<OrderShipToInfoEntity, OrderShipToInfo>();

            CreateMap<OrderForm, Order>();

            CreateMap<Order, OrderForm>();

            CreateMap<PriceInfo, OrderItemPriceInfo>();
        }
    }
}
