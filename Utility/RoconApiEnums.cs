﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCOUploadProcess.Utility
{
        /// <summary>
        /// Delivery types accepted by ROCON for Agency/LCO
        /// </summary>
        //public enum DeliveryType { PickUp, ShipToAddrOnFile };
        public enum DeliveryType { PICK_UP, SHIP_TO_ADDR_ON_FILE };

    /// <summary>
    /// New Order, Cancel Order, Return Order
    /// </summary>
    public enum OrderEvent { Cancel, New, Return};

        /// <summary>
        /// Identify the source system of the order capture - W for webstore, O for woocommerce, C
        /// for eCommerce WCS
        /// </summary>
        public enum OrderSource { C, O, W };
        
        /// <summary>
        ///  Identity LCO Order Status to be populated by ROCON in response
        /// </summary>
        public enum OrderStatus { W, D, C, O, E };

        /// <summary>
        /// Payment methods accepted by ROCON for Agency/LCO
        /// </summary>
        public enum PaymentMethod { CashOnDelivery, Check };

        /// <summary>
        /// to be populated by ROCON in response
        /// </summary>
        public enum Indicator { N, Y };

    ///<summary>
    /// Delivery Charge Type in ROCON
    /// </summary>
    public enum DeliveryChargeType { CalculateDelivery, FreeDelivery, PickUp};
 

    /// <summary>
    /// to capture different error messages for ordered Items
    /// </summary>
    /// "INVALID_ORDER", "PRICE_ERROR", "INVENTORY_ERROR", "SKU_ERROR", "MISSING_ELEMENT", "WRONG_FORMAT", "UNRECOGNIZED_ELEMENT", "FILE_CREATE_ERROR", "FILE_DROP_ERROR", "OTHER"]
    public enum OrderItemErrorType {
        INVALID_ORDER, PRICE_ERROR=100, INVENTORY_ERROR, SKU_ERROR,
        MISSING_ELEMENT, WRONG_FORMAT, UNRECOGNIZED_ELEMENT,
        FILE_CREATE_ERROR, FILE_DROP_ERROR, OTHER }
   

    public enum ErrorType { INVALID_ORDER=100, INVALID_VALUE, PRICE_ERROR, INVENTORY_ERROR, SKU_ERROR, MISSING_ELEMENT,
                                WRONG_FORMAT, UNRECOGNIZED_ELEMENT, FILE_CREATE_ERROR, FILE_DROP_ERROR, OTHER
                                };


    public enum OrderType { AGY, LIC, REG, LCO}

    //CONSIDERATION TO ADD NEW CUSTOMER TYPE = LCO TO BE SPECIFIC TO LCO PROGRAM
    public enum CustomerType { AGENCY, LICENSEE, REGULAR}
   
}
