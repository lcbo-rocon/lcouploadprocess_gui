using System;

namespace LCOUploadProcess.Utility
{
    public class LcboException : Exception
    {
        public LcboException()
        {
        }

        public LcboException(string message)
            : base(message)
        {
        }

        public LcboException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}
