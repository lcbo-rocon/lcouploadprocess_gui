
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Text.RegularExpressions;
using LCOUploadProcess.Domains;
using LCOUploadProcess.Models;
using Serilog;

namespace LCOUploadProcess.Utility
{
    public class Utils
    {
        private static List<string> SEARCH_FIELDS = new List<string>()
                                                        {
                                                            "field",
                                                            "type",
                                                            "operator",
                                                            "value"
                                                        };
        public static string getCurrentWindowsUser(string windowsUser,
                                                    string httpContextUser,
                                                    string userIdentity)
        {
            var currentWinUser = windowsUser.Replace("LCBO\\", "");
            Log.Information($"==> Windows identity name: [{currentWinUser}]");

            Log.Information($"==> User indentity: [{userIdentity}]");
            Log.Information($"==> HttpContext [{httpContextUser}]");

            if (httpContextUser != null)
            {
                Log.Information("Setting user from httpcontext ...");
                currentWinUser = httpContextUser.Replace("LCBO\\", "");
                Log.Information($"Current user is: {currentWinUser}");
            }
            return currentWinUser.ToUpper();
        }

        public static string transformSearchQuery(string searchQuery)
        {
            string newSearchQuery = "";
            string[] values = searchQuery.Split('&');
            for (int i = 0; i < values.Length; ++i)
            {
                var value = values[i];
                // logger.Info("\n" + value);
                if (values[i].Contains("between"))
                {
                    var result = values[i + 2].Split('=')[1];
                    value = value + "&" + values[i + 1] + "," + result;
                    // logger.Info("\n" + i + 1 + " " + value);
                    i += 2;
                }
                newSearchQuery = newSearchQuery + "&" + value;
            }
            return newSearchQuery.TrimStart('&');
        }

        public static string orderBy(W2UIForm w2UIForm,
                                        string orderBy,
                                        string defaultValue,
                                        Dictionary<string, string> fieldToColumnNameDict)
        {
            if (!string.IsNullOrEmpty(w2UIForm.sortField))
            {

                if (string.IsNullOrEmpty(w2UIForm.sortField))
                {
                    Log.Error($"---- orderBy Sort field {w2UIForm.sortField} was not found");
                    throw new LcboException("SortField is Null or Emtpy");
                }
                else
                {
                    var result = String.Format(orderBy, w2UIForm.sortField, w2UIForm.sortDirection);
                    Log.Information($"\n ===> OrderBy is: {result}");
                    return result;
                }
            }
            return defaultValue;
        }

        public static void populateSearcObjs(
                                             W2UIForm w2UIForm,
                                             NameValueCollection values)
        {
            var searchFields = new HashSet<SearchObj>();
            var counterSearch = 0;
            var searchObj = new SearchObj();

            string pattern = @"search\[(\d+)]\[(\w+)\]";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);

            for (int n = 0; n < values.Count - 1; n++)
            {
                Match match = regex.Match(values.GetKey(n));
                string value = values.GetValues(n)[0];

                while (match.Success)
                {
                    for (int i = 2; i < 3; i++)
                    {
                        var group = match.Groups[i];

                        if (!SEARCH_FIELDS.Contains(group.ToString()))
                        {
                            // ignore it ...
                            continue;
                        }

                        Log.Information($"Group key|value({i} => ): [{group.ToString()}|{value}]");

                        // taking the field value
                        if (string.Equals("field", group.ToString()))
                        {
                            // take the value from the key/value dictionary
                            searchObj.field = value;
                        }
                        else if (string.Equals("type", group.ToString()))
                        {
                            // taking type value
                            searchObj.type = value;
                        }
                        else if (string.Equals("operator", group.ToString()))
                        {
                            // taking operator
                            searchObj.operation = value;
                        }
                        else if (string.Equals("value", group.ToString()))
                        {
                            // taking the value
                            searchObj.value = Utils.transformOrElse(value, searchObj);
                        }

                        counterSearch++;
                        if (counterSearch > 3)
                        {
                            // logger.Info($"Creating new object ... {counterSearch} and object is: {searchObj}");
                            counterSearch = 0;
                            // ignore empty value
                            if (!String.IsNullOrEmpty(searchObj.value))
                            {
                                searchFields.Add(searchObj);
                                searchObj = new SearchObj();
                            }
                        }
                    }
                    match = match.NextMatch();
                }
            }
            w2UIForm.searchObjs = searchFields;
        }


        private static string transformOrElse(string value, SearchObj searchObj)
        {
            var result = value;

            if (value.Equals("true", StringComparison.InvariantCultureIgnoreCase)
                || value.Equals("y", StringComparison.InvariantCultureIgnoreCase)
                || value.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
            {
                result = "Y";
            }
            else if (value.Equals("false", StringComparison.InvariantCultureIgnoreCase)
                || value.Equals("n", StringComparison.InvariantCultureIgnoreCase)
                || value.Equals("No", StringComparison.InvariantCultureIgnoreCase))
            {
                result = "N";
            }

            return result.Trim();
        }


        public static string getLastUpdateSqlDateStr()
        {
            var lastUpdateDateStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            return $"TO_DATE('{lastUpdateDateStr}', 'YYYY-MM-DD HH24:mi:ss')";
        }


        public static double oracleReaderToDouble(object oracleDataReaderObj)
        {
            if (DBNull.Value.Equals(oracleDataReaderObj))
            {
                return 0.0;
            }
            return (double) oracleDataReaderObj;
        }


        public static int oracleReaderDecimalToInt(object oracleDataReaderObj)
        {
            if (DBNull.Value.Equals(oracleDataReaderObj))
            {
                return 0;
            }
            return Decimal.ToInt32((decimal)oracleDataReaderObj);
        }


        public static string boolToString(bool value)
        {
            return intToString(boolToInt(value));
        }


        public static int boolToInt(bool value)
        {
            int result = 0;
            if (value)
            {
                result = 1;
            }
            return result;
        }


        public static string intToString(int active)
        {
            var isActive = "N";
            if (active == 1)
            {
                isActive = "Y";
            }
            return isActive;
        }


        public static string oracleReaderToString(object oracleDataReaderObj)
        {
            if (DBNull.Value.Equals(oracleDataReaderObj))
            {
                return "";
            }
            return ((string)oracleDataReaderObj).Trim();
        }


        public static string oracleReaderToDateTimeString(object oracleDataReaderObj, string format)
        {
            if (DBNull.Value.Equals(oracleDataReaderObj))
            {
                return "";
            }
            return ((DateTime)oracleDataReaderObj).ToString(format);

        }


        public static bool isADate(string inputValue, string[] formats)
        {
            if (inputValue.Contains(","))
            {
                inputValue = inputValue.Split(',')[0];
            }
            DateTime dateValue;
            return DateTime.TryParseExact(inputValue, formats, CultureInfo.InvariantCulture, DateTimeStyles.None,
                out dateValue);
        }


        public static string getGeneralErrorMessage()
        {
            return $"Sorry, it appears that you do not have access to this site. Please contact ASGN_B2B for assistance.";
        }

        public static void deleteFiles(string fullPath)
        {
            var files = Directory.GetFiles(fullPath);

            foreach(string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                if(fileInfo.LastAccessTime < DateTime.Now.AddDays(-1))
                {
                    fileInfo.Delete();
                }
            }
        }
    }
}