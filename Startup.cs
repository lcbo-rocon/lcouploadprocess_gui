using System;
using AutoMapper;
using LCOUploadProcess.Services;
using LCOUploadProcess.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
//using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.AspNetCore.Server.IISIntegration;
using Serilog;
using Serilog.Events;
using System.Collections.Generic;

namespace LCOUploadProcess
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            setupLogin(Configuration);

            bool isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            Log.Information($"==> Running in Development mode?: {isDevelopment}");

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IAuthorizationManager, AuthorizationManager>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ILcoUserConfigService, LcoUserConfigService>();
            services.AddScoped<ILCOUpdateService, LCOUpdateService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddSingleton<IRoconEnvironmentService, RoconEnvironmentService>();
            //services.AddControllers();

            //services.AddAuthentication(HttpSysDefaults.AuthenticationScheme);
            //change to use IIS Integration instead of HttpSys
            services.Configure<IISOptions>(options => 
            {
                options.AutomaticAuthentication = true;
            });
            services.AddAuthentication(IISDefaults.AuthenticationScheme);
            services.AddMvc();
            services.AddAutoMapper(
                options => options.AddProfile<MapperProfile>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new List<string> { "index.html" } });
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void setupLogin(IConfiguration config) 
        {
            var loggingFilePath = config["Serilog:filepath"];
            var outputTemplate = config["Serilog:outputTemplate"];
            var rollingInterval = config["Serilog:rollingInterval"];
            var fileSizeLimitBytes = Int32.Parse(config["Serilog:fileSizeLimitBytes"]);
            var minimumLevel = config["Serilog:minimumLevel"];

            var level = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), minimumLevel);

            var shouldLog = Boolean.Parse(config["shouldLog"]);

            if (shouldLog)
            {
                Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Is(level)
                            .WriteTo.Console()
                            .WriteTo.File(loggingFilePath,
                                          outputTemplate: outputTemplate,
                                          fileSizeLimitBytes: fileSizeLimitBytes,
                                          rollingInterval: (RollingInterval)Enum.Parse(typeof(RollingInterval), rollingInterval),
                                          restrictedToMinimumLevel: level)
                            .CreateLogger();
            }
            else
            {
                Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Is(level)
                            .WriteTo.Console()
                            .CreateLogger();
            }
        }
    }
}
