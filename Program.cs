
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.Extensions.Hosting;
using System;

namespace LCOUploadProcess
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            bool isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            Console.WriteLine($"==> Running in Development mode?: {isDevelopment}");
            // if(isDevelopment)
            // {
                // return Host.CreateDefaultBuilder(args)
                // .ConfigureWebHostDefaults(webBuilder =>
                // {
                //     webBuilder.UseStartup<Startup>();
                // });
            // }

            //IIS doesn't like this setting HttpsSys options for some reason
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    // webBuilder.UseStartup<Startup>().UseHttpSys(options =>
                    // {
                    //     options.Authentication.Schemes = 
                    //         AuthenticationSchemes.Basic| 
                    //         AuthenticationSchemes.Negotiate;
                    //     options.Authentication.AllowAnonymous = false;
                    // });
                });
            
        }
    }
}
